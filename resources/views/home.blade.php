@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
		<h3>Estadísticas</h3>
	</div>
</div>

<?php 
foreach ($totales as $total)
{
?>
<div class="row">

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h4 style="font-size:17px;"><strong><?php echo $total->totalingreso;?></strong></h4>
                  <p>Compras</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="{{url('compras/ingreso')}}" class="small-box-footer">Compras <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h4 style="font-size:17px;"><strong><?php echo $total->totalventa;?></strong></h4>
                  <p>Ventas</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="{{url('ventas/venta')}}" class="small-box-footer">Ventas <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->

            <div class="col-lg-4 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h4 style="font-size:17px;"><strong><?php echo $total->totalventa;?></strong></h4>
                  <p>Caja Actual</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="#" class="small-box-footer">Caja <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div><!-- ./col -->
        </div>
<?php }?>

 


@push ('scripts') 
 
@endpush
@endsection