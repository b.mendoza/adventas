@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Datos de la Compañia</h3>
		@if (session('success'))
			<br>
			<div class="alert alert-success">
				{{ session('success') }}
			</div> 
		@endif 
	</div>
</div>
<br>
 
<div class="row">
	<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
		{!!Form::open(array('url'=>'seguridad/company','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}} 
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
			<div class="form-group">
					<label for="d">Código de domicilio fiscal</label>
					@if(!is_null($company))
						<input type="text" required value="{{$company->codigo_del_domicilio_fiscal}}" name="codigo_del_domicilio_fiscal" id="d" class="form-control">
					@else
						<input type="text" required  name="codigo_del_domicilio_fiscal" id="d" class="form-control">
					@endif
				</div>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
				<div class="form-group">
					<label for="a">Código de país</label>
					@if(!is_null($company))
						<input type="text" required value="{{$company->codigo_pais}}" name="codigo_pais" id="a" class="form-control" >
					 @else
						<input type="text" required  name="codigo_pais" id="d" class="form-control">
					@endif
				</div>
		</div>

		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
				<div class="form-group">
					<label for="a">Ubigeo</label>
					@if(!is_null($company))
						<input type="text" required value="{{$company->ubigeo}}" name="ubigeo" id="a" class="form-control" >
					 @else
						<input type="text" required  name="ubigeo" id="d" class="form-control">
					@endif
				</div>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
				<div class="form-group">
					<label for="a">Dirección</label>
					@if(!is_null($company))
						<input type="text" required value="{{$company->direccion}}" name="direccion" id="a" class="form-control" >
					 @else
					 	<input type="text" required  name="direccion" id="d" class="form-control">
					@endif
				</div>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
				<div class="form-group">
					<label for="a">Email</label>
					@if(!is_null($company))
						<input type="email" required value="{{$company->correo_electronico}}" name="correo_electronico" id="a" class="form-control" >
					 @else
						<input type="email" required  name="correo_electronico" id="d" class="form-control">
					@endif
				</div>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
				<div class="form-group">
					<label for="a">Teléfono</label>
					@if(!is_null($company))
						<input type="number" required value="{{$company->telefono}}" name="telefono" id="a" class="form-control">
					 @else
						<input type="number" required  name="telefono" id="d" class="form-control">
					@endif
				</div>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
			<div class="form-group">
					<label for="d">Dominio</label>
					@if(!is_null($company))
						<input type="text" value="{{$company->domain}}" name="domain" id="d" class="form-control" >
					 @else
						<input type="text" required  name="domain" id="d" class="form-control">
					@endif
				</div>
		</div>
		<div class="col-lg-4 col-sm-4 col-md-4 col-xs-4">
				<div class="form-group">
					<label for="a">Api Token</label>
					@if(!is_null($company))
						<input type="text" value="{{$company->api_token}}" name="api_token" id="a" class="form-control">
					 @else
						<input type="text" required  name="api_token" id="d" class="form-control">
					@endif
				</div>
		</div>
		<div class="col-lg-12 col-sm-12 col-md-12 col-xs-12" id="guardar">
				<div class="form-group">
					<input name"_token"   value="{{ csrf_token() }}" type="hidden"></input>
					<button class="btn btn-primary" type="submit">Guardar</button> 
				</div>
		</div>
		{!!Form::close()!!}	
	</div>  
</div>

@push ('scripts')
<script>
$('#liAcceso').addClass("treeview active");
$('#liCompany').addClass("active");
</script>
@endpush
@endsection