@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <h3>Nota Electrónica</h3> 
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
			@endif
		</div>
	</div>
    {!!Form::open(array('url'=>'ventas/venta','method'=>'POST','autocomplete'=>'off'))!!}
        {{Form::token()}}
            <input type="hidden"    name="id"  value="{{$venta->idventa}}" class="form-control"  >
            <input type="hidden"    name="document"  value="{{$venta->number}}" class="form-control"  >
            <input type="hidden"    name="code"  value="{{$venta->tipo_comprobante}}" class="form-control"  >

    <div class="row">
    	<div class="col-lg-5 col-sm-5 col-md-5 col-xs-5">
    		<div class="form-group">
            	<label for="cliente">Cliente</label>
            	<select name="idcliente" id="idcliente" class="form-control selectpicker" data-live-search="true">
                     <option value="{{ $venta->persona->idpersona }}">{{$venta->persona->nombre}}</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                <div class="form-group">
                    <label for="desc">Descripción</label>
                    <input type="text"  required name="description" id="desc" class="form-control"  >
                </div>
            </div>
    	<div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
    		<div class="form-group">
    			<label>Tipo Comprobante</label>
    			<select name="tipo_comprobante" id="tipo_comprobante" class="form-control">
                       <option value="07">Nota de Crédito</option>
                       <option value="08">Nota de Débito</option> 
    			</select>
    		</div>
        </div>
        <div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
                <div class="form-group">
                    <label for="tipo_nota">Tipo de nota</label> 
    
                    <select name="tipo_nota_c" id="tn_nc"  class="form-control">
                        @foreach($note_credit_types as $nc)
                            <option value="{{$nc->id}}">{{$nc->description}}</option>  
                        @endforeach
                    </select>
    
                    <select name="tipo_nota_d"  id="tn_nd" class="form-control">
                        @foreach($note_debit_types as $nd)
                            <option value="{{$nd->id}}">{{$nd->description}}</option>  
                        @endforeach
                    </select>  
    
                </div>
        </div>
    	<div class="col-lg-3 col-sm-3 col-md-3 col-xs-12">
            <div class="form-group">
                <label for="serie_comprobante">Serie Comprobante</label> 
    			<select name="serie_comprobante" value="{{old('serie_comprobante')}}" id="serie_comprobante" class="form-control">
                    @foreach ($series as $serie)
                        <option id="{{$serie->document_type}}" value="{{$serie->number}}">{{$serie->number}}</option>                         
                    @endforeach
                </select>
            </div>
        </div>
        
        <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
            <div class="form-group">
                <label for="impuesto">Impuesto</label>
                <input type="checkbox" checked value="1" name="impuesto" id="impuesto" class="checkbox">18% Impuesto
            </div>
        </div>
    </div>
    <div class="row col-lg-12 col-sm-12 col-md-12 col-xs-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="col-lg-4 col-sm-4 col-md-4 col-xs-12">
                    <div class="form-group">
                        <label>Artículo</label>
                        <select name="pidarticulo" class="form-control selectpicker" id="pidarticulo" data-live-search="true">
                            @foreach($articulos as $articulo)
                            <option value="{{$articulo->idarticulo}}_{{$articulo->stock}}_{{$articulo->precio_promedio}}">{{$articulo->articulo}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                    <div class="form-group">
                        <label for="cantidad">Cantidad</label>
                        <input type="number" name="pcantidad" id="pcantidad" class="form-control" 
                        placeholder="cantidad">
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                    <div class="form-group">
                        <label for="stock">Stock</label>
                        <input type="number" disabled name="pstock" id="pstock" class="form-control" 
                        placeholder="Stock">
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                    <div class="form-group">
                        <label for="precio_venta">Precio venta</label>
                        <input type="number" disabled name="pprecio_venta" id="pprecio_venta" class="form-control" 
                        placeholder="P. venta">
                    </div>
                </div>
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                    <div class="form-group">
                        <label for="descuento">Descuento</label>
                        <input type="number" name="pdescuento" id="pdescuento" class="form-control" 
                        placeholder="Descuento" value="0">
                    </div>
                </div> 
                
                <div class="col-lg-2 col-sm-2 col-md-2 col-xs-12">
                    <div class="form-group">
                       <button type="button" id="bt_add" class="btn btn-primary">Agregar</button>
                    </div>
                </div>

                <div class="col-lg-12 col-sm-12 col-md-12 col-xs-12">
                    <table id="detalles" class="table table-striped table-bordered table-condensed table-hover">
                        <thead style="background-color:#A9D0F5">
                            <th>Opciones</th>
                            <th>Artículo</th>
                            <th>Cantidad</th>
                            <th>Precio Venta</th>
                            <th>Descuento</th>
                            <th>Subtotal</th>
                        </thead>
 
                        <tfoot>

                            <tr>
                                <th  colspan="5"><p align="right">TOTAL:</p></th>
                                <th><p align="right"><span id="total">S/. 0.00</span> <input type="hidden" name="total_venta" id="total_venta"></p></th>
                            </tr>
                            <tr>
                                <th colspan="5"><p align="right">TOTAL IMPUESTO (18%):</p></th>
                                <th><p align="right"><span id="total_impuesto">S/. 0.00</span></p></th>
                            </tr>
                            <tr>
                                <th  colspan="5"><p align="right">TOTAL PAGAR:</p></th>
                                <th><p align="right"><span align="right" id="total_pagar">S/. 0.00</span></p></th>
                            </tr>

                        </tfoot>
                        <tbody>
                            
                        </tbody>
                    </table>
                 </div>
            </div>
        </div>
    	<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12" id="guardar">
    		<div class="form-group">
            	<input name"_token" value="{{ csrf_token() }}" type="hidden"></input>
                <button class="btn btn-primary" type="submit">Guardar</button>
            	<button class="btn btn-danger" type="reset">Cancelar</button>
            </div>
    	</div>
    </div>   
    {!!Form::close()!!}		

@push ('scripts')
<script>
  $(document).ready(function(){
    $('#bt_add').click(function(){
      agregar();
    });
    agregarDetalle(); 
    $("#tn_nd").hide();
  });

  function agregarDetalle()
  {

    var detalle = <?php echo $venta->detalleventa ?>;   

    for(var i=0; i< detalle.length; i++){

        idarticulo = detalle[i].idarticulo;
        articulo= detalle[i].articulo.nombre;
        cantidad= detalle[i].cantidad;

        descuento = detalle[i].descuento;
        precio_venta = detalle[i].precio_venta;
        stock = detalle[i].articulo.stock;

        if (idarticulo!="" && cantidad!="" && cantidad>0 && descuento!="" && precio_venta!="")
        {
            if (parseInt(stock)>=parseInt(cantidad))
            {
            subtotal[cont]=(cantidad*precio_venta-descuento);
            total=total+subtotal[cont];
            var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td><td><input type="number" name="cantidad[]" value="'+cantidad+'"></td><td><input type="number" name="precio_venta[]" value="'+parseFloat(precio_venta).toFixed(2)+'"></td><td><input type="number" name="descuento[]" value="'+parseFloat(descuento).toFixed(2)+'"></td><td align="right">S/. '+parseFloat(subtotal[cont]).toFixed(2)+'</td></tr>';


            cont++;
            limpiar();
            totales();
            evaluar();
            $('#detalles').append(fila);   
            }
            else
            {
                alert ('La cantidad a vender supera el stock');
            }
            
        }
        else
        {
            alert("Error al ingresar el detalle de la venta, revise los datos del artículo");
        }
    }


  }
  

  var cont=0;
  total=0;
  subtotal=[];
  $("#guardar").hide();
  $("#pidarticulo").change(mostrarValores); 
  

  $("#tipo_comprobante").change(tipo_nota);

  function mostrarValores()
  {
    datosArticulo=document.getElementById('pidarticulo').value.split('_');
 
    $("#pprecio_venta").val(datosArticulo[2]);
    $("#pstock").val(datosArticulo[1]);    
  }

  function tipo_nota()
  {
    tipo_comprobante=$("#tipo_comprobante").val();
 
    if (tipo_comprobante=="07")
    {
        marcarSerie();
        $("#tn_nc").show(); 
        $("#tn_nd").hide(); 
    }
    else
    {
        marcarSerie();
        $("#tn_nc").hide(); 
        $("#tn_nd").show(); 
    }
  }

  function marcarSerie()
  {
        tipo_comprobante=$("#tipo_comprobante").val();
        
        if (tipo_comprobante=="07")
        {            
            obtenerSerie("07");
        }
        else
        {           
            obtenerSerie("08");
        }
  }

  function obtenerSerie($type){
    $("#serie_comprobante").each(function(i, obj) {
        for(var i=0;i<obj.length;i++){
            if(obj[i].id==$type){ 
                $("#serie_comprobante").val(obj[i].value);  
            }
        }             
    });
  }


  function agregar()
  {
    datosArticulo=document.getElementById('pidarticulo').value.split('_');

    idarticulo=datosArticulo[0];
    articulo=$("#pidarticulo option:selected").text();
    cantidad=$("#pcantidad").val();

    descuento=$("#pdescuento").val();
    precio_venta=$("#pprecio_venta").val();
    stock=$("#pstock").val();

    if (idarticulo!="" && cantidad!="" && cantidad>0 && descuento!="" && precio_venta!="")
    {
        if (parseInt(stock)>=parseInt(cantidad))
        {
        subtotal[cont]=(cantidad*precio_venta-descuento);
        total=total+subtotal[cont];

        var fila='<tr class="selected" id="fila'+cont+'"><td><button type="button" class="btn btn-warning" onclick="eliminar('+cont+');">X</button></td><td><input type="hidden" name="idarticulo[]" value="'+idarticulo+'">'+articulo+'</td><td><input type="number" name="cantidad[]" value="'+cantidad+'"></td><td><input type="number" name="precio_venta[]" value="'+parseFloat(precio_venta).toFixed(2)+'"></td><td><input type="number" name="descuento[]" value="'+parseFloat(descuento).toFixed(2)+'"></td><td align="right">S/. '+parseFloat(subtotal[cont]).toFixed(2)+'</td></tr>';
        cont++;
        limpiar();
        totales();
        evaluar();
        $('#detalles').append(fila);   
        }
        else
        {
            alert ('La cantidad a vender supera el stock');
        }
        
    }
    else
    {
        alert("Error al ingresar el detalle de la venta, revise los datos del artículo");
    }
  }
  
  function limpiar(){
    $("#pcantidad").val("");
    $("#pdescuento").val("0");
    $("#pprecio_venta").val("");
  }

  function totales()
  {
        $("#total").html("S/. " + total.toFixed(2));
        $("#total_venta").val(total.toFixed(2));
        
        //Calcumos el impuesto
        if ($("#impuesto").is(":checked"))
        {
            por_impuesto=18;
        }
        else
        {
            por_impuesto=0;   
        }
        total_impuesto=total*por_impuesto/100;
        total_pagar=total+total_impuesto;
        $("#total_impuesto").html("S/. " + total_impuesto.toFixed(2));
        $("#total_pagar").html("S/. " + total_pagar.toFixed(2));
        
  }

  function evaluar()
  {
    if (total>0)
    {
      $("#guardar").show();
    }
    else
    {
      $("#guardar").hide(); 
    }
   }

   function eliminar(index){
    total=total-subtotal[index]; 
    totales();  
    $("#fila" + index).remove();
    evaluar();

  }
$('#liVentas').addClass("treeview active");
$('#liVentass').addClass("active");
  
</script>
@endpush
@endsection