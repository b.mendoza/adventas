@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
		<h3>Listado de Ventas <a href="venta/create"><button class="btn btn-success">Nuevo</button></a> <a href="{{url('reporteventas')}}" target="_blank"><button class="btn btn-info">Reporte</button></a></h3>
		@include('ventas.venta.search')
		@if (session('success'))
			<br>
			<div class="alert alert-info">
				{{ session('success') }}
			</div>
			<br>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>Fecha</th>
					<th>Cliente</th>
					<th>Tipo comprobante</th>
					<th>Comprobante</th>
					<th>Impuesto</th>
					<th>Subtotal</th>
					<th>Total</th>
					<th>Estado</th>
					<th class="text-center">Descargas</th>
					<th>Opciones</th>
				</thead>
			   @foreach ($ventas as $ven)
					@if($ven->annulment==1)
						<tr class="text-danger">
					@else
						<tr>
					@endif
					<td>{{ $ven->fecha_hora}}</td>
					<td>{{ $ven->nombre}}</td>
					<td> {{$document_type[$ven->tipo_comprobante]}} </td>
					<td> {{$ven->number}}  </td>
					<td>{{ $ven->impuesto}}</td>
					<td>{{ $ven->total_venta}}</td>
					<td>{{ $ven->total_venta*1.18}}</td>
					<td>{{ $ven->estado}}</td>
					<td>
						@if($ven->success)
							<a href="{{$ven->have_pdf}}" target="_blank"><button class="btn btn-sm btn-danger">PDF</button></a>
							<a href="{{$ven->have_xml}}" target="_blank"><button class="btn btn-sm btn-primary">XML</button></a>
							
							@if ($ven->have_cdr)
								<a href="{{$ven->have_cdr}}" target="_blank"><button class="btn btn-sm btn-success">CDR</button></a>								
							@endif
							
						@endif
						<a href="{{URL::action('VentaController@show',$ven->idventa)}}"><button class="btn btn-sm btn-primary">Detalles</button></a>
					</td>
					<td> 
						@if($ven->tipo_comprobante == "01" && $ven->annulment!=1)
							<a href="{{'/voided/annulment/'}}{{$ven->idventa}}"   ><button class="btn btn-sm btn-danger">Anular</button></a>
							<a href="{{'/note/'}}{{$ven->idventa}}"  ><button class="btn btn-sm btn-primary">Nota</button></a>
						@elseif($ven->tipo_comprobante == "03" && $ven->annulment!=1 && $ven->send_summary)
							<a href="{{'/voided/annulment/'}}{{$ven->idventa}}"   ><button class="btn btn-sm btn-danger">Anular</button></a> 
						@endif
					</td>
				</tr>
				@include('ventas.venta.modal')
				@endforeach
			</table>
		</div>
		{{$ventas->render()}}
	</div>
</div>
@push ('scripts')
<script>
$('#liVentas').addClass("treeview active");
$('#liVentass').addClass("active");
</script>
@endpush

@endsection