@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Anulaciones </h3>
		
		@if (session('success'))
			<br>
			<div class="alert alert-info">
				{{ session('success') }}
			</div>
			<br>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>#</th>
					<th>External ID</th>
					<th>Fecha de emisión</th>
					<th>Fecha de referencia</th>
					<th>Documento Afectado</th>
					<th>Ticket</th>
					<th>Descargas</th>
					<th>Opciones</th>
				</thead>
               @foreach ($annulments as $k=>$a)
				<tr>
					<td>{{ $k+1 }}</td>
					<td>{{ $a->external_id}}</td>
					<td> {{$a->date_of_issue}} </td>
					<td>{{ $a->date_of_reference}}</td>
					<td>{{ $a->document_affected}}</td>
					<td>{{ $a->ticket}}</td>
					<td>
						@if($a->success) 
							<a href="{{$a->have_xml}}" target="_blank"><button class="btn btn-sm btn-primary">XML</button></a>
							<a href="{{$a->have_cdr}}" target="_blank"><button class="btn btn-sm btn-success">CDR</button></a>					
							
						@endif

					</td>
					<td>
						@if(!$a->success)
						{!!Form::open(array('url'=>'annulment/ticket','method'=>'POST','autocomplete'=>'off'))!!}
							{{Form::token()}}
							<input type="hidden" value="{{$a->id}}" name="id">  
							<input type="hidden" value="{{$a->type_document_affected}}" name="type">  
							<button class="btn btn-sm btn-primary" type="submit">Consultar</button> 
						{!!Form::close()!!}
			 
						@endif
					</td>
				</tr>  

				@endforeach
			</table>
		</div>
		{{$annulments->render()}}
	</div>
</div>
@push ('scripts')
<script>
$('#liVentas').addClass("treeview active");
$('#liAnulaciones').addClass("active");
</script>
@endpush

@endsection