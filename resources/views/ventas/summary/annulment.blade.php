@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <h3>Anulación de documento</h3>
            <br>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
            @endif
            

            @if (session('success'))
                <div class="alert alert-info">
                    {{ session('success') }}
                </div>
                <br>
            @endif
		</div>
	</div>

    {!!Form::open(array('url'=> 'annulment','method'=>'POST','autocomplete'=>'off'))!!}
          
			<input name"_token" value="{{ csrf_token() }}" type="hidden"></input>
			<input type="hidden" name="type" value="{{$type_document_affected['type']}}">
			<input type="hidden" name="annulment" value="{{$annulment}}">
            <div class="row">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="form-group">
						<label for="descripcion">Motivo de la anulación</label>
						<input type="text" name="motive_annulment" class="form-control" placeholder="">
					</div>
				</div>								
            
			</div>
			<div class="row">
				<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
					<div class="form-group">
						<button class="btn btn-primary" type="submit">Anular</button>
						<button class="btn btn-danger" type="reset">Cancelar</button>
					</div>
				</div>
			</div>
    
                
	{!!Form::close()!!}		


@push ('scripts') 
<script>
	
    $('#liVentas').addClass("treeview active");
    $('#liAnulaciones').addClass("active");

</script>
@endpush
@endsection