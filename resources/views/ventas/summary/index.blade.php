@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<h3>Resúmenes <a href="summary/create"><button class="btn btn-success">Nuevo</button></a></h3>
		
		@if (session('success'))
			<br>
			<div class="alert alert-info">
				{{ session('success') }}
			</div>
			<br>
		@endif
	</div>
</div>

<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<div class="table-responsive">
			<table class="table table-striped table-bordered table-condensed table-hover">
				<thead>
					<th>#</th>
					<th>External ID</th>
					<th>Fecha de emisión</th>
					<th>Fecha de referencia</th>
					<th>Ticket</th>
					<th>Descargas</th>   
				</thead>
               @foreach ($summaries as $k=>$s)
					@if($s->annulment==1)
						<tr class="text-danger">
					@else
						<tr>
					@endif
					<td>{{ $k+1 }}</td>
					<td>{{ $s->external_id}}</td>
					<td> {{$s->date_of_issue}} </td>
					<td>{{ $s->date_of_reference}}</td>
					<td>{{ $s->ticket}}</td>
					<td>
						@if($s->success) 
							<a href="{{$s->have_xml}}" target="_blank"><button class="btn btn-sm btn-primary">XML</button></a>
							<a href="{{$s->have_cdr}}" target="_blank"><button class="btn btn-sm btn-success">CDR</button></a>					
						@else
							{!!Form::open(array('url'=>'summary/ticket','method'=>'POST','autocomplete'=>'off'))!!}
								{{Form::token()}}
								<input type="hidden" value="{{$s->id}}" name="id">  
								<button class="btn btn-sm btn-primary" type="submit">Consultar</button> 
							{!!Form::close()!!}  
						@endif
						 
					</td>
					
				</tr>  

				@endforeach
			</table>
		</div>
		{{$summaries->render()}}
	</div>
</div>
@push ('scripts')
<script>
$('#liVentas').addClass("treeview active");
$('#liResumenes').addClass("active");
</script>
@endpush

@endsection