@extends ('layouts.admin')
@section ('contenido')
	<div class="row">
		<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <h3>Nuevo Resúmen</h3>
            <br>
			@if (count($errors)>0)
			<div class="alert alert-danger">
				<ul>
				@foreach ($errors->all() as $error)
					<li>{{$error}}</li>
				@endforeach
				</ul>
			</div>
            @endif
            

            @if (session('success'))
                <div class="alert alert-info">
                    {{ session('success') }}
                </div>
                <br>
            @endif
		</div>
	</div>
    {!!Form::open(array('url'=>'summary','method'=>'POST','autocomplete'=>'off'))!!}
            {{Form::token()}}
            <input name"_token" value="{{ csrf_token() }}" type="hidden"></input>
            <div class="row">
                <div class="col-lg-6 col-sm-6 col-md-6 col-xs-6">
                    <div class="row">
                        <div class="col-md-6">
                                <div class="form-group"> 
                        
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="date_search" required class="form-control pull-right" id="datepicker">
                                        </div> 
                                    </div>
                        </div>
                        <div class="col-md-6">
                        <button class="btn btn-primary" type="submit">Generar resúmen</button> 

                        </div>
                    </div>
                        

                </div>
            
            </div>
    
                
			{!!Form::close()!!}		


@push ('scripts') 
<script>
    
    $('#datepicker').datepicker({
        format: 'yyyy-mm-dd', 
    });

    $('#liVentas').addClass("treeview active");
    $('#liResumenes').addClass("active");

</script>
@endpush
@endsection