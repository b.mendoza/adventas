@extends ('layouts.admin')
@section ('contenido')
<div class="row">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="row">
      <div class="col-lg-8" >
       <p style="font-size: 24px;">Listado de Categorías </p>  
      </div>
      <div class="col-lg-4 text-right" >  
          <a href="categoria/create"> <button class="btn btn-success"> <i class="fa fa-plus-square" aria-hidden="true"></i>  Nuevo</button></a>
          <a href="{{url('reportecategorias')}}" target="_blank"><button class="btn btn-info"> <i class="fa fa-file" aria-hidden="true"></i>  Generar Reporte</button></a>
      </div>
    </div>
		
      <br>
      <br> 
	</div>
</div>


<div  
class="dataTables_wrapper form-inline dt-bootstrap">
 
 

<div class="row">
  <div class="col-sm-12">
    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                      <tr role="row">
                        <th class="sorting_asc" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending" style="width: 3px;">N°</th>
                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending" style="width: 202px;">Descripción</th>
                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending" style="width: 178px;">Detalle</th>
                        <th class="sorting" tabindex="0" aria-controls="example1" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending" style="width: 139px;">Opciones</th> 
                    </thead>
                    <tbody>
                      
                      
                      @foreach ($categorias as $cat)
                        <tr>
                          <td>{{ $cat->idcategoria}}</td>
                          <td>{{ $cat->nombre}}</td>
                          <td>{{ $cat->descripcion}}</td>
                          <td> 
                            <a href="{{URL::action('CategoriaController@edit',$cat->idcategoria)}}" >  
                              <button class="btn btn-warning "  data-toggle="tooltip" title="" data-original-title="Editar">  
                                <i class="fa fa-pencil-square" aria-hidden="true"></i>
                              </button>
                            </a>

                            <a href="" data-target="#modal-delete-{{$cat->idcategoria}}" data-toggle="modal">
                              <button class="btn btn-danger"  data-toggle="tooltip" title="" data-original-title="Eliminar"> 
                                <i class="fa fa-trash" aria-hidden="true"></i>  
                              </button>
                            </a>
                          </td>
                        </tr>
                        @include('almacen.categoria.modal')
                      @endforeach  
                  </tbody> 

                  </table>
</div>
    
</div>




@push ('scripts')
<script>
$('#liAlmacen').addClass("treeview active");
$('#liCategorias').addClass("active");
</script>
@endpush
@endsection