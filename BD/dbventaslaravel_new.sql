/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : dbventaslaravel

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-01-09 17:53:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for annulments
-- ----------------------------
DROP TABLE IF EXISTS `annulments`;
CREATE TABLE `annulments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) DEFAULT NULL,
  `date_of_issue` date DEFAULT NULL,
  `date_of_reference` date DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `have_cdr` varchar(255) DEFAULT NULL,
  `have_xml` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `success` varchar(20) DEFAULT NULL,
  `type_document_affected` varchar(255) DEFAULT NULL,
  `document_affected` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of annulments
-- ----------------------------
INSERT INTO `annulments` VALUES ('1', '5ff22acf-a285-4113-b191-25f12c4104c5', '2019-01-09', '2019-01-09', '1547045706171', null, null, null, null, 'venta', 'F001-7');
INSERT INTO `annulments` VALUES ('2', '0e480e9a-ef80-426e-bd00-9fc804cf67ad', '2019-01-09', '2019-01-09', '1547045992845', null, null, null, null, 'venta', 'F001-7');
INSERT INTO `annulments` VALUES ('3', '036654fa-69b7-4e60-81b7-cf3185962e28', '2019-01-09', '2019-01-09', '1547046344147', null, null, null, null, 'summary', 'RC-20190109-8,');
INSERT INTO `annulments` VALUES ('4', '92926308-23a1-496d-b71a-ad8885515b40', '2019-01-09', '2019-01-09', '1547046521073', null, null, null, null, 'summary', 'RC-20190109-11');
INSERT INTO `annulments` VALUES ('5', '47446938-bf0a-4848-86ed-26a891b518a3', '2019-01-09', '2019-01-09', '1547046666636', null, null, null, null, 'summary', 'RC-20190109-13');
INSERT INTO `annulments` VALUES ('6', '363471f9-a12d-42da-b741-2c6e7edba609', '2019-01-09', '2019-01-09', '1547046687285', null, null, null, null, 'venta', 'F001-7');
INSERT INTO `annulments` VALUES ('7', 'a51f9110-e24e-41e8-ae97-0a7fc75a35c1', '2019-01-09', '2019-01-09', '1547047745515', 'http://apifacturaloperu.com/downloads/summary/cdr/a51f9110-e24e-41e8-ae97-0a7fc75a35c1', 'http://apifacturaloperu.com/downloads/summary/xml/a51f9110-e24e-41e8-ae97-0a7fc75a35c1', 'El Resumen diario RC-20190109-20, ha sido aceptado', '1', 'summary', 'RC-20190109-19');
INSERT INTO `annulments` VALUES ('8', 'ba150945-bc10-4ff3-800d-e23871d8f404', '2019-01-09', '2019-01-09', '1547047780769', 'http://apifacturaloperu.com/downloads/voided/cdr/ba150945-bc10-4ff3-800d-e23871d8f404', 'http://apifacturaloperu.com/downloads/voided/xml/ba150945-bc10-4ff3-800d-e23871d8f404', 'La Comunicacion de baja RA-20190109-12, ha sido aceptada', '1', 'venta', 'F001-8');
INSERT INTO `annulments` VALUES ('9', '3c355f4f-214e-46bc-b556-7472ec54a407', '2019-01-09', '2019-01-09', '1547048377824', 'http://apifacturaloperu.com/downloads/voided/cdr/3c355f4f-214e-46bc-b556-7472ec54a407', 'http://apifacturaloperu.com/downloads/voided/xml/3c355f4f-214e-46bc-b556-7472ec54a407', 'La Comunicacion de baja RA-20190109-13, ha sido aceptada', '1', 'venta', 'F001-8');
INSERT INTO `annulments` VALUES ('10', '4aef3acf-9b0d-48bc-81be-dee9df0def7d', '2019-01-09', '2019-01-09', '1547048524206', 'http://apifacturaloperu.com/downloads/voided/cdr/4aef3acf-9b0d-48bc-81be-dee9df0def7d', 'http://apifacturaloperu.com/downloads/voided/xml/4aef3acf-9b0d-48bc-81be-dee9df0def7d', 'La Comunicacion de baja RA-20190109-14, ha sido aceptada', '1', 'venta', 'F001-8');
INSERT INTO `annulments` VALUES ('11', '4c3847d4-3d28-42e1-99ea-048d4deb10a9', '2019-01-09', '2019-01-09', '1547048593946', 'http://apifacturaloperu.com/downloads/voided/cdr/4c3847d4-3d28-42e1-99ea-048d4deb10a9', 'http://apifacturaloperu.com/downloads/voided/xml/4c3847d4-3d28-42e1-99ea-048d4deb10a9', 'La Comunicacion de baja RA-20190109-15, ha sido aceptada', '1', 'venta', 'F001-8');
INSERT INTO `annulments` VALUES ('12', '56c3d66c-d243-46a7-a6f6-9eea306e666f', '2019-01-09', '2019-01-09', '1547048655817', 'http://apifacturaloperu.com/downloads/voided/cdr/56c3d66c-d243-46a7-a6f6-9eea306e666f', 'http://apifacturaloperu.com/downloads/voided/xml/56c3d66c-d243-46a7-a6f6-9eea306e666f', 'La Comunicacion de baja RA-20190109-16, ha sido aceptada', '1', 'venta', 'F001-8');
INSERT INTO `annulments` VALUES ('13', 'e0d832c7-ba5a-49cb-86e2-c0fbb28e70e8', '2019-01-09', '2019-01-09', '1547048696577', 'http://apifacturaloperu.com/downloads/summary/cdr/e0d832c7-ba5a-49cb-86e2-c0fbb28e70e8', 'http://apifacturaloperu.com/downloads/summary/xml/e0d832c7-ba5a-49cb-86e2-c0fbb28e70e8', 'El Resumen diario RC-20190109-22, ha sido aceptado', '1', 'summary', 'RC-20190109-21');
INSERT INTO `annulments` VALUES ('14', '65220c7f-a335-420a-9c35-adbfd5bc08ae', '2019-01-09', '2019-01-09', '1547049386939', 'http://apifacturaloperu.com/downloads/voided/cdr/65220c7f-a335-420a-9c35-adbfd5bc08ae', 'http://apifacturaloperu.com/downloads/voided/xml/65220c7f-a335-420a-9c35-adbfd5bc08ae', 'La Comunicacion de baja RA-20190109-17, ha sido aceptada', '1', 'venta', 'F001-7');
INSERT INTO `annulments` VALUES ('15', '4df03d4a-b002-4a33-be11-3a800a056260', '2019-01-09', '2019-01-09', '1547065079106', 'http://apifacturaloperu.com/downloads/voided/cdr/4df03d4a-b002-4a33-be11-3a800a056260', 'http://apifacturaloperu.com/downloads/voided/xml/4df03d4a-b002-4a33-be11-3a800a056260', 'La Comunicacion de baja RA-20190109-20, ha sido aceptada', '1', 'venta', 'FC01-22');
INSERT INTO `annulments` VALUES ('16', '460751ee-d1f5-42c6-954d-615936b94bff', '2019-01-09', '2019-01-09', '1547065500280', 'http://apifacturaloperu.com/downloads/voided/cdr/460751ee-d1f5-42c6-954d-615936b94bff', 'http://apifacturaloperu.com/downloads/voided/xml/460751ee-d1f5-42c6-954d-615936b94bff', 'La Comunicacion de baja RA-20190109-21, ha sido aceptada', '1', 'venta', 'F001-14');
INSERT INTO `annulments` VALUES ('17', '9cddfff6-7ce9-4ed8-8ddc-7f0c3309623b', '2019-01-09', '2019-01-09', '1547065599262', 'http://apifacturaloperu.com/downloads/voided/cdr/9cddfff6-7ce9-4ed8-8ddc-7f0c3309623b', 'http://apifacturaloperu.com/downloads/voided/xml/9cddfff6-7ce9-4ed8-8ddc-7f0c3309623b', 'La Comunicacion de baja RA-20190109-22, ha sido aceptada', '1', 'venta', 'FC01-23');
INSERT INTO `annulments` VALUES ('18', '4106f145-e31a-4e73-935c-dfc4fb86d075', '2019-01-09', '2019-01-09', '1547065728785', 'http://apifacturaloperu.com/downloads/voided/cdr/4106f145-e31a-4e73-935c-dfc4fb86d075', 'http://apifacturaloperu.com/downloads/voided/xml/4106f145-e31a-4e73-935c-dfc4fb86d075', 'La Comunicacion de baja RA-20190109-23, ha sido aceptada', '1', 'venta', 'F001-15');
INSERT INTO `annulments` VALUES ('19', '4d17aa3d-4823-4f55-aed4-5bbed6c1e837', '2019-01-09', '2019-01-09', '1547069092876', 'http://apifacturaloperu.com/downloads/voided/cdr/4d17aa3d-4823-4f55-aed4-5bbed6c1e837', 'http://apifacturaloperu.com/downloads/voided/xml/4d17aa3d-4823-4f55-aed4-5bbed6c1e837', 'La Comunicacion de baja RA-20190109-24, ha sido aceptada', '1', 'venta', 'F001-16');
INSERT INTO `annulments` VALUES ('20', '60935fe7-dcc2-4ecf-a9bd-ea8858303736', '2019-01-09', '2019-01-09', '1547069806583', 'http://apifacturaloperu.com/downloads/voided/cdr/60935fe7-dcc2-4ecf-a9bd-ea8858303736', 'http://apifacturaloperu.com/downloads/voided/xml/60935fe7-dcc2-4ecf-a9bd-ea8858303736', 'La Comunicacion de baja RA-20190109-25, ha sido aceptada', '1', 'venta', 'F001-17');
INSERT INTO `annulments` VALUES ('21', 'f020e7b0-9659-4d62-a060-0925cfec2b0e', '2019-01-09', '2019-01-09', '1547069891050', 'http://apifacturaloperu.com/downloads/voided/cdr/f020e7b0-9659-4d62-a060-0925cfec2b0e', 'http://apifacturaloperu.com/downloads/voided/xml/f020e7b0-9659-4d62-a060-0925cfec2b0e', 'La Comunicacion de baja RA-20190109-26, ha sido aceptada', '1', 'venta', 'FC01-26');

-- ----------------------------
-- Table structure for articulo
-- ----------------------------
DROP TABLE IF EXISTS `articulo`;
CREATE TABLE `articulo` (
  `idarticulo` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `descripcion` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idarticulo`),
  KEY `fk_articulo_categoria_idx` (`idcategoria`),
  CONSTRAINT `fk_articulo_categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Records of articulo
-- ----------------------------
INSERT INTO `articulo` VALUES ('1', '1', '7701234000011', 'Impresora Epson Lx200', '16', '', 'impresora.jpg', 'Activo');
INSERT INTO `articulo` VALUES ('2', '1', '7702004003508', 'Impresora Epson M300', '19', '', 'Impresora Epson.jpeg', 'Activo');
INSERT INTO `articulo` VALUES ('4', '3', '5901234123457', 'Cable UTP Cat-5', '89', '', 'descarga.jpg', 'Activo');
INSERT INTO `articulo` VALUES ('5', '3', '8412345678905', 'Cable VGA 2Mt', '52', '', 'images.jpg', 'Activo');

-- ----------------------------
-- Table structure for categoria
-- ----------------------------
DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Records of categoria
-- ----------------------------
INSERT INTO `categoria` VALUES ('1', 'Equipos Cómputo', 'Accesorios de cómputo', '1');
INSERT INTO `categoria` VALUES ('2', 'Útiles', 'Útiles', '1');
INSERT INTO `categoria` VALUES ('3', 'Limpieza', 'Artículos de limpieza', '1');
INSERT INTO `categoria` VALUES ('4', 'Medicina', 'Artículos medicinales', '1');
INSERT INTO `categoria` VALUES ('5', 'Líquidos', 'Líquidos', '1');
INSERT INTO `categoria` VALUES ('6', 'Comida', 'productos de comida', '1');
INSERT INTO `categoria` VALUES ('7', 'Vestimenta', 'Artículos de vestimenta', '1');
INSERT INTO `categoria` VALUES ('8', 'Servicios', 'Servicios', '1');
INSERT INTO `categoria` VALUES ('9', 'Cables', 'Todo tipo de cables', '1');
INSERT INTO `categoria` VALUES ('10', 'Accesorios de Sonido 2', 'Todos los accesorios de sonido 2', '0');
INSERT INTO `categoria` VALUES ('11', 'fgj1', 'fjghf2', '0');
INSERT INTO `categoria` VALUES ('12', 'hjkhk2', 'khykuyh', '0');
INSERT INTO `categoria` VALUES ('13', 'qqqqqq', 'wwww', '0');
INSERT INTO `categoria` VALUES ('14', 'Tecnología', 'Artículos de cómputo', '1');
INSERT INTO `categoria` VALUES ('15', 'Jabón líquilo', 'Productos de Aseo', '1');
INSERT INTO `categoria` VALUES ('16', 'Hoja Bón', 'Artículos de Oficina', '1');

-- ----------------------------
-- Table structure for companies
-- ----------------------------
DROP TABLE IF EXISTS `companies`;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `codigo_del_domicilio_fiscal` varchar(255) DEFAULT NULL,
  `codigo_pais` varchar(255) DEFAULT NULL,
  `ubigeo` varchar(255) DEFAULT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `correo_electronico` varchar(255) DEFAULT NULL,
  `telefono` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of companies
-- ----------------------------
INSERT INTO `companies` VALUES ('0', '0000', 'PE', '150101', 'Av. 2 de Mayo', 'demo@gmail.com', '411-5522');

-- ----------------------------
-- Table structure for configuration
-- ----------------------------
DROP TABLE IF EXISTS `configuration`;
CREATE TABLE `configuration` (
  `id` int(11) NOT NULL,
  `domain` varchar(255) DEFAULT NULL,
  `api_token` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of configuration
-- ----------------------------
INSERT INTO `configuration` VALUES ('1', 'http://apifacturaloperu.com', 'jocFml34KlpOZomg3ha41wqqs02zNbwsVcNc8E2p9N0ez5NHEL');

-- ----------------------------
-- Table structure for detalle_ingreso
-- ----------------------------
DROP TABLE IF EXISTS `detalle_ingreso`;
CREATE TABLE `detalle_ingreso` (
  `iddetalle_ingreso` int(11) NOT NULL AUTO_INCREMENT,
  `idingreso` int(11) NOT NULL,
  `idarticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_compra` decimal(11,2) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  PRIMARY KEY (`iddetalle_ingreso`),
  KEY `fk_detalle_ingreso_idx` (`idingreso`),
  KEY `fk_detalle_ingreso_articulo_idx` (`idarticulo`),
  CONSTRAINT `fk_detalle_ingreso` FOREIGN KEY (`idingreso`) REFERENCES `ingreso` (`idingreso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_ingreso_articulo` FOREIGN KEY (`idarticulo`) REFERENCES `articulo` (`idarticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Records of detalle_ingreso
-- ----------------------------
INSERT INTO `detalle_ingreso` VALUES ('18', '13', '1', '10', '500.00', '600.00');
INSERT INTO `detalle_ingreso` VALUES ('19', '13', '4', '100', '1.00', '2.00');
INSERT INTO `detalle_ingreso` VALUES ('20', '13', '5', '50', '10.00', '15.00');
INSERT INTO `detalle_ingreso` VALUES ('21', '14', '1', '1', '10.00', '15.00');
INSERT INTO `detalle_ingreso` VALUES ('22', '15', '1', '1', '10.00', '15.00');
INSERT INTO `detalle_ingreso` VALUES ('23', '16', '1', '1', '10.00', '15.00');
INSERT INTO `detalle_ingreso` VALUES ('24', '17', '2', '10', '150.00', '200.00');
INSERT INTO `detalle_ingreso` VALUES ('25', '18', '2', '10', '5.00', '10.00');
INSERT INTO `detalle_ingreso` VALUES ('26', '19', '2', '1', '25.00', '30.00');
INSERT INTO `detalle_ingreso` VALUES ('27', '20', '1', '1', '20.00', '30.00');
INSERT INTO `detalle_ingreso` VALUES ('28', '20', '2', '1', '20.00', '25.00');
INSERT INTO `detalle_ingreso` VALUES ('29', '21', '1', '10', '10.00', '25.00');
INSERT INTO `detalle_ingreso` VALUES ('30', '22', '2', '2', '20.00', '30.00');
INSERT INTO `detalle_ingreso` VALUES ('31', '23', '5', '10', '25.00', '30.00');

-- ----------------------------
-- Table structure for detalle_venta
-- ----------------------------
DROP TABLE IF EXISTS `detalle_venta`;
CREATE TABLE `detalle_venta` (
  `iddetalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `idventa` int(11) NOT NULL,
  `idarticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  `descuento` decimal(11,2) NOT NULL,
  PRIMARY KEY (`iddetalle_venta`),
  KEY `fk_detalle_venta_articulo_idx` (`idarticulo`),
  KEY `fk_detalle_venta_idx` (`idventa`),
  CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`idventa`) REFERENCES `venta` (`idventa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_venta_articulo` FOREIGN KEY (`idarticulo`) REFERENCES `articulo` (`idarticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Records of detalle_venta
-- ----------------------------
INSERT INTO `detalle_venta` VALUES ('1', '1', '1', '10', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('2', '4', '2', '1', '303.60', '0.00');
INSERT INTO `detalle_venta` VALUES ('3', '5', '4', '1', '6.60', '0.00');
INSERT INTO `detalle_venta` VALUES ('4', '6', '1', '1', '52.60', '0.60');
INSERT INTO `detalle_venta` VALUES ('5', '6', '2', '1', '303.60', '0.30');
INSERT INTO `detalle_venta` VALUES ('6', '7', '5', '1', '25.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('7', '8', '4', '37', '6.60', '0.00');
INSERT INTO `detalle_venta` VALUES ('8', '8', '5', '18', '25.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('9', '8', '2', '45', '303.60', '0.00');
INSERT INTO `detalle_venta` VALUES ('10', '8', '1', '41', '52.60', '0.00');
INSERT INTO `detalle_venta` VALUES ('11', '9', '5', '1', '25.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('12', '9', '4', '1', '6.60', '0.00');
INSERT INTO `detalle_venta` VALUES ('13', '10', '5', '2', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('14', '10', '1', '1', '600.00', '50.00');
INSERT INTO `detalle_venta` VALUES ('15', '11', '5', '1', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('16', '12', '5', '1', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('17', '13', '5', '2', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('18', '13', '4', '5', '2.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('19', '14', '1', '1', '161.25', '0.00');
INSERT INTO `detalle_venta` VALUES ('20', '15', '2', '5', '200.00', '5.00');
INSERT INTO `detalle_venta` VALUES ('21', '16', '1', '1', '135.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('22', '17', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('23', '18', '4', '2', '2.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('24', '19', '1', '2', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('25', '19', '5', '2', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('26', '19', '4', '1', '2.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('27', '20', '1', '2', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('28', '20', '4', '3', '2.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('29', '21', '1', '1', '116.67', '1.00');
INSERT INTO `detalle_venta` VALUES ('30', '22', '2', '10', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('31', '23', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('32', '24', '1', '2', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('33', '24', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('34', '25', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('35', '26', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('38', '29', '1', '2', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('64', '55', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('66', '57', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('69', '60', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('71', '62', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('72', '63', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('75', '66', '1', '2', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('76', '67', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('81', '72', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('82', '73', '1', '2', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('83', '73', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('84', '74', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('85', '74', '5', '2', '22.50', '0.00');
INSERT INTO `detalle_venta` VALUES ('86', '75', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('87', '76', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('88', '77', '1', '2', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('89', '78', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('90', '79', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('91', '80', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('92', '81', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('93', '82', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('94', '83', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('95', '84', '2', '3', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('96', '85', '2', '2', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('97', '86', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('98', '87', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('99', '88', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('100', '89', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('101', '90', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('102', '91', '1', '1', '116.67', '0.00');
INSERT INTO `detalle_venta` VALUES ('103', '92', '2', '1', '59.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('104', '93', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('105', '93', '5', '1', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('125', '103', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('126', '103', '5', '1', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('127', '104', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('128', '104', '5', '1', '15.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('129', '104', '2', '1', '200.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('132', '106', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('134', '108', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('136', '110', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('146', '120', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('147', '121', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('148', '122', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('149', '123', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('150', '124', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('151', '125', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('152', '126', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('153', '127', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('154', '128', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('155', '129', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('156', '130', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('157', '131', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('158', '132', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('159', '132', '2', '1', '200.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('160', '133', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('161', '134', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('162', '134', '2', '2', '200.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('163', '135', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('164', '136', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('165', '136', '2', '1', '200.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('166', '137', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('167', '138', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('168', '139', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('169', '140', '2', '1', '200.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('170', '141', '2', '1', '200.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('171', '142', '2', '1', '200.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('172', '143', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('173', '144', '1', '1', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('174', '145', '1', '2', '600.00', '0.00');
INSERT INTO `detalle_venta` VALUES ('175', '146', '1', '2', '600.00', '0.00');

-- ----------------------------
-- Table structure for ingreso
-- ----------------------------
DROP TABLE IF EXISTS `ingreso`;
CREATE TABLE `ingreso` (
  `idingreso` int(11) NOT NULL AUTO_INCREMENT,
  `idproveedor` int(11) NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `num_comprobante` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idingreso`),
  KEY `fk_ingreso_persona_idx` (`idproveedor`),
  CONSTRAINT `fk_ingreso_persona` FOREIGN KEY (`idproveedor`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Records of ingreso
-- ----------------------------
INSERT INTO `ingreso` VALUES ('3', '5', 'Boleta', '001', '001', '2016-08-19 22:19:34', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('4', '6', 'Boleta', '001', '0001', '2016-08-30 16:19:48', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('5', '5', 'Factura', '001', '00004', '2016-08-30 16:29:00', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('6', '5', 'Boleta', '001', '0001', '2016-08-31 11:02:43', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('7', '5', 'Boleta', '001', '0008', '2016-08-31 11:22:33', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('8', '5', 'Boleta', '001', '0001', '2016-08-31 11:37:20', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('9', '5', 'Boleta', '007', '0008', '2016-08-31 15:37:16', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('10', '6', 'Factura', '001', '001', '2016-09-01 16:33:03', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('11', '5', 'Boleta', '001', '0003', '2016-09-01 17:00:51', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('12', '5', 'Boleta', '001', '00077', '2016-09-27 15:19:58', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('13', '5', 'Boleta', '001', '00002', '2016-09-29 16:12:58', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('14', '5', 'Boleta', '001', '0002', '2016-10-02 13:42:22', '0.00', 'A');
INSERT INTO `ingreso` VALUES ('15', '5', 'Boleta', '002', '0003', '2016-10-02 13:43:51', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('16', '5', 'Ticket', '001', '0005', '2016-10-02 13:44:13', '0.00', 'A');
INSERT INTO `ingreso` VALUES ('17', '5', 'Boleta', '001', '000155', '2016-11-01 10:38:48', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('18', '6', 'Boleta', '0009', '123', '2018-02-19 21:08:22', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('19', '6', 'Boleta', '001', '2545', '2018-06-05 14:42:23', '18.00', 'C');
INSERT INTO `ingreso` VALUES ('20', '5', 'Boleta', '001', '4525', '2018-06-05 14:43:09', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('21', '5', 'Boleta', '0002', '2545', '2018-06-05 14:46:14', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('22', '5', 'Boleta', '001', '4525', '2018-06-20 13:56:38', '18.00', 'A');
INSERT INTO `ingreso` VALUES ('23', '6', 'Boleta', '0001', '741', '2019-01-07 04:01:43', '18.00', 'A');

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2014_10_12_100000_create_password_resets_table', '1');

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `venta_id` int(10) unsigned NOT NULL,
  `note_type` enum('credit','debit') COLLATE utf8mb4_unicode_ci NOT NULL,
  `note_credit_type_id` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `note_debit_type_id` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `affected_document` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of notes
-- ----------------------------
INSERT INTO `notes` VALUES ('11', '321', 'credit', '01', null, 'Error al emitir comprobante', '308');
INSERT INTO `notes` VALUES ('15', '106', '', '01', '', 'sdffdsfd', '0');
INSERT INTO `notes` VALUES ('16', '106', '', '', '02', 'sdas', '0');
INSERT INTO `notes` VALUES ('17', '124', '', '01', '', 'rghgf', 'FC01-3');
INSERT INTO `notes` VALUES ('18', '127', '', '01', '', 'sds', 'F001-12');
INSERT INTO `notes` VALUES ('19', '129', '', '01', '', 'errror', 'F001-14');
INSERT INTO `notes` VALUES ('20', '131', '', '', '03', 'gff', 'F001-15');
INSERT INTO `notes` VALUES ('21', '131', '', '', '10', 'erorres', 'F001-15');
INSERT INTO `notes` VALUES ('22', '135', '', '', '10', 'ddasaa', 'B001-6');
INSERT INTO `notes` VALUES ('23', '135', '', '04', '', 'sfddfd', 'B001-6');
INSERT INTO `notes` VALUES ('24', '135', '', '', '11', 'sdfdffsd', 'B001-6');
INSERT INTO `notes` VALUES ('25', '143', '', '02', '', 'error', 'F001-18');
INSERT INTO `notes` VALUES ('26', '145', '', '01', '', 'ffddfs', 'B001-14');

-- ----------------------------
-- Table structure for note_credit_types
-- ----------------------------
DROP TABLE IF EXISTS `note_credit_types`;
CREATE TABLE `note_credit_types` (
  `id` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  KEY `note_credit_types_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of note_credit_types
-- ----------------------------
INSERT INTO `note_credit_types` VALUES ('01', 'Anulación de la operación', '1');
INSERT INTO `note_credit_types` VALUES ('02', 'Anulación por error en el RUC', '1');
INSERT INTO `note_credit_types` VALUES ('03', 'Corrección por error en la descripción', '1');
INSERT INTO `note_credit_types` VALUES ('04', 'Descuento global', '1');
INSERT INTO `note_credit_types` VALUES ('05', 'Descuento por ítem', '1');
INSERT INTO `note_credit_types` VALUES ('06', 'Devolución total', '1');
INSERT INTO `note_credit_types` VALUES ('07', 'Devolución por ítem', '1');
INSERT INTO `note_credit_types` VALUES ('08', 'Bonificación', '1');
INSERT INTO `note_credit_types` VALUES ('09', 'Disminución en el valor', '1');
INSERT INTO `note_credit_types` VALUES ('10', 'Otros Conceptos', '1');
INSERT INTO `note_credit_types` VALUES ('11', 'Ajustes de operaciones de exportación', '1');
INSERT INTO `note_credit_types` VALUES ('12', 'Ajustes afectos al IVAP', '1');

-- ----------------------------
-- Table structure for note_debit_types
-- ----------------------------
DROP TABLE IF EXISTS `note_debit_types`;
CREATE TABLE `note_debit_types` (
  `id` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL,
  KEY `note_debit_types_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of note_debit_types
-- ----------------------------
INSERT INTO `note_debit_types` VALUES ('01', 'Intereses por mora', '1');
INSERT INTO `note_debit_types` VALUES ('02', 'Aumento en el valor', '1');
INSERT INTO `note_debit_types` VALUES ('03', 'Penalidades/ otros conceptos', '1');
INSERT INTO `note_debit_types` VALUES ('10', 'Ajustes de operaciones de exportación', '1');
INSERT INTO `note_debit_types` VALUES ('11', 'Ajustes afectos al IVAP', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for persona
-- ----------------------------
DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_persona` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_documento` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `num_documento` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Records of persona
-- ----------------------------
INSERT INTO `persona` VALUES ('1', 'Cliente', 'Ana Montenegro', '6', '10433900495', 'Margaritas 1368 - Chiclayo', '96325871', 'ana@gmail.com');
INSERT INTO `persona` VALUES ('2', 'Cliente', 'Juan Perez', '1', '36985214', 'Margaritas 1368 - Chiclayo', '96325871', 'juan@gmail.com');
INSERT INTO `persona` VALUES ('3', 'Cliente', 'Jose Martinez', '1', '0174589632', 'José Gálvez 1368 - Trujillo', '96325871', 'jose@gmail.com');
INSERT INTO `persona` VALUES ('4', 'Cliente', 'Juan Carlos Arcila', 'DNI', '47715777', 'José Gálvez 1368', '', 'jcarlos.ad7@gmail.com');
INSERT INTO `persona` VALUES ('5', 'Proveedor', 'Soluciones Innovadoras Perú S.A.C', 'RUC', '20600121234', 'Chiclayo 0123', '931742904', 'informes.solinperu@gmail.com');
INSERT INTO `persona` VALUES ('6', 'Proveedor', 'Inversiones SantaAna S.A.C', 'RUC', '20546231478', 'Chongoyape 01', '074963258', 'santaana@gmail.com');
INSERT INTO `persona` VALUES ('7', 'Cliente', 'Luis Cesar Tafur Torres', 'DNI', '48124556', 'PS7 MZ. N LT. 5 AH. LAMPA DE ORO VENTANILLA- CALLAO 06 CALLAO,', '924899800', 'ingenieroluistafur@gmail.com');

-- ----------------------------
-- Table structure for summaries
-- ----------------------------
DROP TABLE IF EXISTS `summaries`;
CREATE TABLE `summaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) DEFAULT NULL,
  `number` varchar(255) DEFAULT NULL,
  `date_of_issue` date DEFAULT NULL,
  `date_of_reference` date DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `have_cdr` varchar(255) DEFAULT NULL,
  `have_xml` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `success` varchar(20) DEFAULT NULL,
  `annulment` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of summaries
-- ----------------------------
INSERT INTO `summaries` VALUES ('1', 'd64cd984-17aa-4c57-b985-ab5b1607c35a', null, '2019-01-08', '2019-01-08', '1546977236053', 'http://apifacturaloperu.com/downloads/summary/cdr/d64cd984-17aa-4c57-b985-ab5b1607c35a', 'http://apifacturaloperu.com/downloads/summary/xml/d64cd984-17aa-4c57-b985-ab5b1607c35a', 'El Resumen diario RC-20190108-1, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('2', '71f4d587-43b9-4c2b-b267-21b26307561b', null, '2019-01-08', '2019-01-08', '1546977450574', 'http://apifacturaloperu.com/downloads/summary/cdr/71f4d587-43b9-4c2b-b267-21b26307561b', 'http://apifacturaloperu.com/downloads/summary/xml/71f4d587-43b9-4c2b-b267-21b26307561b', 'El Resumen diario RC-20190108-2, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('3', '32a108f8-de74-44cc-bf52-5cb412b3ab87', null, '2019-01-08', '2019-01-08', '1546978045050', 'http://apifacturaloperu.com/downloads/summary/cdr/32a108f8-de74-44cc-bf52-5cb412b3ab87', 'http://apifacturaloperu.com/downloads/summary/xml/32a108f8-de74-44cc-bf52-5cb412b3ab87', 'El Resumen diario RC-20190108-8, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('4', '003bfaf2-e343-43d6-b918-12b263109f03', null, '2019-01-08', '2019-01-08', '1546978068464', 'http://apifacturaloperu.com/downloads/summary/cdr/003bfaf2-e343-43d6-b918-12b263109f03', 'http://apifacturaloperu.com/downloads/summary/xml/003bfaf2-e343-43d6-b918-12b263109f03', 'El Resumen diario RC-20190108-9, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('5', '71d16be3-b6e7-41bd-9d3d-8b2fd2ce4740', null, '2019-01-08', '2019-01-08', '1546978147770', 'http://apifacturaloperu.com/downloads/summary/cdr/71d16be3-b6e7-41bd-9d3d-8b2fd2ce4740', 'http://apifacturaloperu.com/downloads/summary/xml/71d16be3-b6e7-41bd-9d3d-8b2fd2ce4740', 'El Resumen diario RC-20190108-10, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('6', '830b10c2-8e5e-48b4-9905-2e8dba6b83ef', null, '2019-01-08', '2019-01-08', '1546979556880', 'http://apifacturaloperu.com/downloads/summary/cdr/830b10c2-8e5e-48b4-9905-2e8dba6b83ef', 'http://apifacturaloperu.com/downloads/summary/xml/830b10c2-8e5e-48b4-9905-2e8dba6b83ef', 'El Resumen diario RC-20190108-12, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('7', '4723a46c-8e04-4382-9f06-f9fa0173e830', null, '2019-01-08', '2019-01-08', '1546979603340', 'http://apifacturaloperu.com/downloads/summary/cdr/4723a46c-8e04-4382-9f06-f9fa0173e830', 'http://apifacturaloperu.com/downloads/summary/xml/4723a46c-8e04-4382-9f06-f9fa0173e830', 'El Resumen diario RC-20190108-13, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('8', 'e9481080-a1d1-4cbb-8d5d-84aabea36692', null, '2019-01-08', '2019-01-08', '1546988526966', 'http://apifacturaloperu.com/downloads/summary/cdr/e9481080-a1d1-4cbb-8d5d-84aabea36692', 'http://apifacturaloperu.com/downloads/summary/xml/e9481080-a1d1-4cbb-8d5d-84aabea36692', 'El Resumen diario RC-20190108-15, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('9', '1b51c989-97eb-4395-bbf1-7bf6e1250be8', null, '2019-01-09', '2019-01-09', '1547043770738', 'http://apifacturaloperu.com/downloads/summary/cdr/1b51c989-97eb-4395-bbf1-7bf6e1250be8', 'http://apifacturaloperu.com/downloads/summary/xml/1b51c989-97eb-4395-bbf1-7bf6e1250be8', 'El Resumen diario RC-20190109-2, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('10', '0fb2eb43-41ad-4361-80d4-af6350bbc493', 'baja', '2019-01-09', '2019-01-09', '1547046235893', 'http://apifacturaloperu.com/downloads/summary/cdr/0fb2eb43-41ad-4361-80d4-af6350bbc493', 'http://apifacturaloperu.com/downloads/summary/xml/0fb2eb43-41ad-4361-80d4-af6350bbc493', 'La Comunicacion de baja RA-20181122-1, ha sido aceptada', '1', null);
INSERT INTO `summaries` VALUES ('11', '47d63e30-97a2-41c0-beb6-bc290131028b', 'RC-20190109-8,', '2019-01-09', '2019-01-09', '1547046320889', 'http://apifacturaloperu.com/downloads/summary/cdr/47d63e30-97a2-41c0-beb6-bc290131028b', 'http://apifacturaloperu.com/downloads/summary/xml/47d63e30-97a2-41c0-beb6-bc290131028b', 'El Resumen diario RC-20190109-8, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('12', 'ca9dc2a5-863b-4a68-b363-7fff5a24ec01', '4-RC-20190109-10', '2019-01-09', '2019-01-09', '1547046472995', 'http://apifacturaloperu.com/downloads/summary/cdr/ca9dc2a5-863b-4a68-b363-7fff5a24ec01', 'http://apifacturaloperu.com/downloads/summary/xml/ca9dc2a5-863b-4a68-b363-7fff5a24ec01', 'El Resumen diario RC-20190109-10, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('13', '9e0a40fa-16f1-4ccd-aec8-44dc691b839b', 'RC-20190109-11', '2019-01-09', '2019-01-09', '1547046501021', 'http://apifacturaloperu.com/downloads/summary/cdr/9e0a40fa-16f1-4ccd-aec8-44dc691b839b', 'http://apifacturaloperu.com/downloads/summary/xml/9e0a40fa-16f1-4ccd-aec8-44dc691b839b', 'El Resumen diario RC-20190109-11, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('14', '93f3d920-f139-4992-8545-e7fc5c829cff', 'RC-20190109-13', '2019-01-09', '2019-01-09', '1547046650389', 'http://apifacturaloperu.com/downloads/summary/cdr/93f3d920-f139-4992-8545-e7fc5c829cff', 'http://apifacturaloperu.com/downloads/summary/xml/93f3d920-f139-4992-8545-e7fc5c829cff', 'El Resumen diario RC-20190109-13, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('15', 'a6341796-cd8a-4cd6-a996-c48733b61d9b', null, '2019-01-09', '2019-01-09', '1547046898436', null, null, null, null, null);
INSERT INTO `summaries` VALUES ('16', '8338c521-372c-4316-b170-799741ca2136', null, '2019-01-09', '2019-01-09', '1547046957764', null, null, null, null, null);
INSERT INTO `summaries` VALUES ('17', '804b7953-3b5c-4b0c-95ac-52d80e1bbdf3', 'RC-20190109-19', '2019-01-09', '2019-01-09', '1547047698657', 'http://apifacturaloperu.com/downloads/summary/cdr/804b7953-3b5c-4b0c-95ac-52d80e1bbdf3', 'http://apifacturaloperu.com/downloads/summary/xml/804b7953-3b5c-4b0c-95ac-52d80e1bbdf3', 'El Resumen diario RC-20190109-19, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('18', 'a32ec50f-f284-421c-8a9a-6ef5d0ac29e7', 'RC-20190109-21', '2019-01-09', '2019-01-09', '1547048684635', 'http://apifacturaloperu.com/downloads/summary/cdr/a32ec50f-f284-421c-8a9a-6ef5d0ac29e7', 'http://apifacturaloperu.com/downloads/summary/xml/a32ec50f-f284-421c-8a9a-6ef5d0ac29e7', 'El Resumen diario RC-20190109-21, ha sido aceptado', '1', '1');
INSERT INTO `summaries` VALUES ('19', 'c7741170-cb3d-4375-ad96-56738da4329b', 'RC-20190109-27', '2019-01-09', '2019-01-08', '1547069506982', 'http://apifacturaloperu.com/downloads/summary/cdr/c7741170-cb3d-4375-ad96-56738da4329b', 'http://apifacturaloperu.com/downloads/summary/xml/c7741170-cb3d-4375-ad96-56738da4329b', 'El Resumen diario RC-20190109-27, ha sido aceptado', '1', null);
INSERT INTO `summaries` VALUES ('20', 'e7e6db1c-87cd-4c34-a126-779a3a1933f4', 'RC-20190109-28', '2019-01-09', '2019-01-08', '1547069554226', 'http://apifacturaloperu.com/downloads/summary/cdr/e7e6db1c-87cd-4c34-a126-779a3a1933f4', 'http://apifacturaloperu.com/downloads/summary/xml/e7e6db1c-87cd-4c34-a126-779a3a1933f4', 'El Resumen diario RC-20190109-28, ha sido aceptado', '1', null);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('3', 'Luis', 'luistafur@gmail.com', '$2y$10$LT67nTsfiL6YPcvmBnKV6OpOFrb3r4L6gSd6czYMQYhIpcH6Ys5Gm', '8Z20fQVOacHIcbDVCaIgJXf4gynAYTF9Crjup5ldiVd6Fq27QstDUAtXZwAv', '2016-10-25 02:37:20', '2019-01-07 05:17:29');

-- ----------------------------
-- Table structure for venta
-- ----------------------------
DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta` (
  `idventa` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8_spanish2_ci NOT NULL,
  `num_comprobante` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `total_venta` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `date_of_issue` date DEFAULT NULL,
  `success` varchar(40) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `number` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `external_id` varchar(200) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `have_xml` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `have_pdf` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `have_cdr` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `response` varchar(255) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `annulment` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`idventa`),
  KEY `fk_venta_cliente_idx` (`idcliente`),
  CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`idcliente`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- ----------------------------
-- Records of venta
-- ----------------------------
INSERT INTO `venta` VALUES ('1', '1', 'Boleta', '001', '0001', '2016-09-01 00:00:00', '18.00', '120.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('3', '1', 'Boleta', '001', '0005', '2016-09-28 15:42:43', '18.00', '303.60', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('4', '1', 'Boleta', '001', '0005', '2016-09-28 15:43:02', '18.00', '303.60', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('5', '3', 'Boleta', '001', '0005', '2016-09-28 15:43:17', '18.00', '6.60', 'C', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('6', '6', 'Boleta', '001', '0002', '2016-09-28 15:44:02', '18.00', '355.30', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('7', '1', 'Boleta', '001', '00002', '2016-09-28 16:16:35', '18.00', '25.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('8', '1', 'Boleta', '007', '777', '2016-09-28 16:18:15', '18.00', '16512.80', 'C', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('9', '4', 'Boleta', '001', '00005', '2016-09-28 22:23:28', '18.00', '31.60', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('10', '3', 'Factura', '001', '0008', '2016-09-29 16:54:13', '18.00', '580.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('11', '1', 'Factura', '001', '0005', '2016-10-02 13:46:13', '18.00', '15.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('12', '1', 'Boleta', '001', '0007', '2016-10-02 13:46:38', '0.00', '15.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('13', '3', 'Factura', '007', '00077', '2016-10-02 14:25:12', '18.00', '40.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('14', '1', 'Factura', '001', '00010', '2016-11-01 10:39:26', '18.00', '161.25', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('15', '4', 'Boleta', '002', '123', '2018-02-19 21:07:24', '0.00', '1000.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('16', '3', 'Ticket', '001', '2545', '2018-06-05 14:44:21', '18.00', '135.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('17', '1', 'Boleta', '002', '2545', '2018-06-05 14:46:45', '0.00', '116.67', 'C', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('18', '1', 'Boleta', '001', '1452', '2018-06-20 13:54:00', '18.00', '4.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('19', '7', 'Boleta', '001', '123', '2019-01-07 03:57:14', '18.00', '250.33', 'C', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('20', '1', 'Boleta', '001', '125', '2019-01-07 04:11:32', '18.00', '239.33', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('21', '1', 'Factura', '', '1', '2019-01-07 17:26:58', '18.00', '115.67', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('22', '1', 'Boleta', '', '4', '2019-01-07 17:52:14', '18.00', '59.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('23', '1', 'Factura', 'F001', '15', '2019-01-08 10:16:45', '18.00', '116.67', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('24', '1', 'Factura', 'F001', '16', '2019-01-08 10:18:43', '18.00', '351.33', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('25', '1', 'Factura', 'F001', '#', '2019-01-08 10:25:07', '18.00', '116.67', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('26', '1', 'Factura', 'F001', '#', '2019-01-08 10:37:25', '18.00', '116.67', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('29', '1', 'Factura', 'F001', '#', '2019-01-08 10:44:47', '18.00', '233.33', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('55', '1', '01', 'F001', '#', '2019-01-08 11:46:24', '18.00', '116.67', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('57', '1', '01', 'F001', '#', '2019-01-08 12:06:56', '18.00', '118.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('60', '1', '01', 'F001', '#', '2019-01-08 12:10:50', '18.00', '59.00', 'A', null, null, '9ff99d08-c424-4c97-8740-6898a4087355', '9ff99d08-c424-4c97-8740-6898a4087355', 'http://apifacturaloperu.com/downloads/document/xml/9ff99d08-c424-4c97-8740-6898a4087355', 'http://apifacturaloperu.com/downloads/document/pdf/9ff99d08-c424-4c97-8740-6898a4087355', 'http://apifacturaloperu.com/downloads/document/cdr/9ff99d08-c424-4c97-8740-6898a4087355', 'La Factura numero F001-17, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('62', '1', '01', 'F001', '#', '2019-01-08 12:18:30', '18.00', '116.67', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('63', '1', '01', 'F001', '#', '2019-01-08 12:19:14', '18.00', '59.00', 'A', null, '1', 'F001-18', '2947e9df-a609-4956-b6c1-caad648dff25', 'http://apifacturaloperu.com/downloads/document/xml/2947e9df-a609-4956-b6c1-caad648dff25', 'http://apifacturaloperu.com/downloads/document/pdf/2947e9df-a609-4956-b6c1-caad648dff25', 'http://apifacturaloperu.com/downloads/document/cdr/2947e9df-a609-4956-b6c1-caad648dff25', 'La Factura numero F001-18, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('66', '1', '03', 'B001', '#', '2019-01-08 12:21:42', '18.00', '233.33', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('67', '4', '03', 'B001', '#', '2019-01-08 12:22:33', '18.00', '59.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('72', '1', '03', 'B001', '#', '2019-01-08 12:27:56', '18.00', '116.67', 'A', null, '1', 'B001-8', '86ad7e4e-6db7-4812-9bb2-3cad6a453ade', 'http://apifacturaloperu.com/downloads/document/xml/86ad7e4e-6db7-4812-9bb2-3cad6a453ade', 'http://apifacturaloperu.com/downloads/document/pdf/86ad7e4e-6db7-4812-9bb2-3cad6a453ade', '', '', null);
INSERT INTO `venta` VALUES ('73', '1', '01', 'F001', '#', '2019-01-08 12:28:48', '18.00', '351.33', 'C', null, '1', 'F001-19', 'a37bd807-921d-4f6a-b233-3dddff470ed2', 'http://apifacturaloperu.com/downloads/document/xml/a37bd807-921d-4f6a-b233-3dddff470ed2', 'http://apifacturaloperu.com/downloads/document/pdf/a37bd807-921d-4f6a-b233-3dddff470ed2', 'http://apifacturaloperu.com/downloads/document/cdr/a37bd807-921d-4f6a-b233-3dddff470ed2', 'La Factura numero F001-19, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('74', '1', '03', 'B001', '#', '2019-01-08 12:30:52', '18.00', '163.00', 'C', null, '1', 'B001-9', 'de957f63-6361-435f-84cc-a33dfeef46d0', 'http://apifacturaloperu.com/downloads/document/xml/de957f63-6361-435f-84cc-a33dfeef46d0', 'http://apifacturaloperu.com/downloads/document/pdf/de957f63-6361-435f-84cc-a33dfeef46d0', '', '', null);
INSERT INTO `venta` VALUES ('75', '1', '01', 'F001', '#', '2019-01-08 17:44:31', '18.00', '118.00', 'A', null, '1', 'F001-18', 'a8ca9a17-100f-4ab8-9815-e06b7759ae35', 'http://apifacturaloperu.com/downloads/document/xml/a8ca9a17-100f-4ab8-9815-e06b7759ae35', 'http://apifacturaloperu.com/downloads/document/pdf/a8ca9a17-100f-4ab8-9815-e06b7759ae35', 'http://apifacturaloperu.com/downloads/document/cdr/a8ca9a17-100f-4ab8-9815-e06b7759ae35', 'La Factura numero F001-18, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('76', '1', '01', 'F001', '#', '2019-01-08 17:45:27', '18.00', '118.00', 'A', null, '1', 'F001-19', 'eb382419-33f1-4632-a1fd-3be984aa610d', 'http://apifacturaloperu.com/downloads/document/xml/eb382419-33f1-4632-a1fd-3be984aa610d', 'http://apifacturaloperu.com/downloads/document/pdf/eb382419-33f1-4632-a1fd-3be984aa610d', 'http://apifacturaloperu.com/downloads/document/cdr/eb382419-33f1-4632-a1fd-3be984aa610d', 'La Factura numero F001-19, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('77', '1', '01', 'F001', '#', '2019-01-08 17:47:14', '18.00', '233.33', 'A', null, '1', 'F001-20', 'a384257b-efe4-4c5e-bc11-d7632fdb8ed2', 'http://apifacturaloperu.com/downloads/document/xml/a384257b-efe4-4c5e-bc11-d7632fdb8ed2', 'http://apifacturaloperu.com/downloads/document/pdf/a384257b-efe4-4c5e-bc11-d7632fdb8ed2', 'http://apifacturaloperu.com/downloads/document/cdr/a384257b-efe4-4c5e-bc11-d7632fdb8ed2', 'La Factura numero F001-20, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('78', '3', '01', 'F001', '#', '2019-01-08 17:47:48', '18.00', '118.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('79', '4', '03', 'B001', '#', '2019-01-08 17:48:08', '0.00', '118.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('80', '1', '01', 'F001', '#', '2019-01-08 17:49:04', '18.00', '118.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('81', '1', '01', 'F001', '#', '2019-01-08 17:50:03', '18.00', '118.00', 'A', null, '1', 'F001-23', '03cf0711-78d2-4a27-8a24-d08312302cc7', 'http://apifacturaloperu.com/downloads/document/xml/03cf0711-78d2-4a27-8a24-d08312302cc7', 'http://apifacturaloperu.com/downloads/document/pdf/03cf0711-78d2-4a27-8a24-d08312302cc7', 'http://apifacturaloperu.com/downloads/document/cdr/03cf0711-78d2-4a27-8a24-d08312302cc7', 'La Factura numero F001-23, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('82', '4', '03', 'B001', '#', '2019-01-08 17:50:43', '0.00', '59.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('83', '2', '03', 'B001', '#', '2019-01-08 17:51:35', '0.00', '118.00', 'A', null, '1', 'B001-7', '9567c8a9-dc1a-418c-91b2-9c4ed985fee7', 'http://apifacturaloperu.com/downloads/document/xml/9567c8a9-dc1a-418c-91b2-9c4ed985fee7', 'http://apifacturaloperu.com/downloads/document/pdf/9567c8a9-dc1a-418c-91b2-9c4ed985fee7', '', '', null);
INSERT INTO `venta` VALUES ('84', '2', '03', 'B001', '#', '2019-01-08 17:52:00', '0.00', '177.00', 'A', null, '1', 'B001-8', '146dbd03-defd-45e0-903c-301fd6e83323', 'http://apifacturaloperu.com/downloads/document/xml/146dbd03-defd-45e0-903c-301fd6e83323', 'http://apifacturaloperu.com/downloads/document/pdf/146dbd03-defd-45e0-903c-301fd6e83323', '', '', null);
INSERT INTO `venta` VALUES ('85', '2', '03', 'B001', '#', '2019-01-08 17:53:54', '0.00', '118.00', 'A', null, '1', 'B001-10', 'c99d8301-6600-47d1-8562-f6d9367f9193', 'http://apifacturaloperu.com/downloads/document/xml/c99d8301-6600-47d1-8562-f6d9367f9193', 'http://apifacturaloperu.com/downloads/document/pdf/c99d8301-6600-47d1-8562-f6d9367f9193', '', 'Boleta B001-10 generada correctamente', null);
INSERT INTO `venta` VALUES ('86', '1', '01', 'F001', '#', '2019-01-08 18:01:26', '18.00', '59.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('87', '1', '01', 'F001', '#', '2019-01-08 18:01:40', '18.00', '59.00', 'A', null, null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('88', '1', '03', 'B001', '#', '2019-01-09 09:22:39', '18.00', '59.00', 'A', null, '1', 'B001-4', '826b3598-66ac-4ff6-a093-b215b4eef1de', 'http://apifacturaloperu.com/downloads/document/xml/826b3598-66ac-4ff6-a093-b215b4eef1de', 'http://apifacturaloperu.com/downloads/document/pdf/826b3598-66ac-4ff6-a093-b215b4eef1de', '', 'Boleta B001-4 generada correctamente', null);
INSERT INTO `venta` VALUES ('89', '1', '01', 'F001', '#', '2019-01-09 09:26:21', '18.00', '59.00', 'A', null, '1', 'F001-5', '55c8d8af-b6d3-4041-8faf-419373b7ff9d', 'http://apifacturaloperu.com/downloads/document/xml/55c8d8af-b6d3-4041-8faf-419373b7ff9d', 'http://apifacturaloperu.com/downloads/document/pdf/55c8d8af-b6d3-4041-8faf-419373b7ff9d', 'http://apifacturaloperu.com/downloads/document/cdr/55c8d8af-b6d3-4041-8faf-419373b7ff9d', 'La Factura numero F001-5, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('90', '1', '01', 'F001', '#', '2019-01-09 09:29:35', '18.00', '116.67', 'A', '2019-01-09', '1', 'F001-6', 'bcbe8ad9-9159-494c-b65f-3c5268ece772', 'http://apifacturaloperu.com/downloads/document/xml/bcbe8ad9-9159-494c-b65f-3c5268ece772', 'http://apifacturaloperu.com/downloads/document/pdf/bcbe8ad9-9159-494c-b65f-3c5268ece772', 'http://apifacturaloperu.com/downloads/document/cdr/bcbe8ad9-9159-494c-b65f-3c5268ece772', 'La Factura numero F001-6, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('91', '1', '01', 'F001', '#', '2019-01-09 09:48:00', '18.00', '116.67', 'A', '2019-01-09', '1', 'F001-7', '1716cdce-e6d3-47be-b41c-e624b8d2db05', 'http://apifacturaloperu.com/downloads/document/xml/1716cdce-e6d3-47be-b41c-e624b8d2db05', 'http://apifacturaloperu.com/downloads/document/pdf/1716cdce-e6d3-47be-b41c-e624b8d2db05', 'http://apifacturaloperu.com/downloads/document/cdr/1716cdce-e6d3-47be-b41c-e624b8d2db05', 'La Factura numero F001-7, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('92', '1', '01', 'F001', '#', '2019-01-09 10:29:34', '18.00', '59.00', 'A', '2019-01-09', '1', 'F001-8', 'bed2c21e-28fe-4232-9954-08fb3e03f144', 'http://apifacturaloperu.com/downloads/document/xml/bed2c21e-28fe-4232-9954-08fb3e03f144', 'http://apifacturaloperu.com/downloads/document/pdf/bed2c21e-28fe-4232-9954-08fb3e03f144', 'http://apifacturaloperu.com/downloads/document/cdr/bed2c21e-28fe-4232-9954-08fb3e03f144', 'La Factura numero F001-8, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('93', '1', '01', 'F001', '#', '2019-01-09 12:38:39', '18.00', '615.00', 'A', '2019-01-09', '1', 'F001-9', 'f7041aa0-6911-4453-8216-5ffc0965cc84', 'http://apifacturaloperu.com/downloads/document/xml/f7041aa0-6911-4453-8216-5ffc0965cc84', 'http://apifacturaloperu.com/downloads/document/pdf/f7041aa0-6911-4453-8216-5ffc0965cc84', 'http://apifacturaloperu.com/downloads/document/cdr/f7041aa0-6911-4453-8216-5ffc0965cc84', 'La Factura numero F001-9, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('103', '1', '07', 'FC01', '#', '2019-01-09 14:40:28', '18.00', '615.00', 'A', '2019-01-09', '1', 'FC01-3', 'cbf9434d-a971-400b-9f40-90ac735cc1cb', 'http://apifacturaloperu.com/downloads/document/xml/cbf9434d-a971-400b-9f40-90ac735cc1cb', 'http://apifacturaloperu.com/downloads/document/pdf/cbf9434d-a971-400b-9f40-90ac735cc1cb', 'http://apifacturaloperu.com/downloads/document/cdr/cbf9434d-a971-400b-9f40-90ac735cc1cb', 'La Nota de Credito numero FC01-3, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('104', '1', '08', 'FD01', '#', '2019-01-09 14:44:08', '18.00', '815.00', 'A', '2019-01-09', '1', 'FD01-1', '091dbd28-2790-4f32-852f-fdddb14071a1', 'http://apifacturaloperu.com/downloads/document/xml/091dbd28-2790-4f32-852f-fdddb14071a1', 'http://apifacturaloperu.com/downloads/document/pdf/091dbd28-2790-4f32-852f-fdddb14071a1', 'http://apifacturaloperu.com/downloads/document/cdr/091dbd28-2790-4f32-852f-fdddb14071a1', 'La Nota de Debito numero FD01-1, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('106', '1', '01', 'F001', '#', '2019-01-09 14:56:10', '18.00', '600.00', 'A', '2019-01-09', '1', 'F001-10', '8ed2f3ae-6f00-4beb-89e3-ac8f0da3767d', 'http://apifacturaloperu.com/downloads/document/xml/8ed2f3ae-6f00-4beb-89e3-ac8f0da3767d', 'http://apifacturaloperu.com/downloads/document/pdf/8ed2f3ae-6f00-4beb-89e3-ac8f0da3767d', 'http://apifacturaloperu.com/downloads/document/cdr/8ed2f3ae-6f00-4beb-89e3-ac8f0da3767d', 'La Factura numero F001-10, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('108', '1', '07', 'FC01', '#', '2019-01-09 14:57:30', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-5', 'd3033aaa-c793-4c0c-ba13-bf35538d9297', 'http://apifacturaloperu.com/downloads/document/xml/d3033aaa-c793-4c0c-ba13-bf35538d9297', 'http://apifacturaloperu.com/downloads/document/pdf/d3033aaa-c793-4c0c-ba13-bf35538d9297', 'http://apifacturaloperu.com/downloads/document/cdr/d3033aaa-c793-4c0c-ba13-bf35538d9297', 'La Nota de Credito numero FC01-5, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('110', '1', '07', 'FC01', '#', '2019-01-09 15:00:19', '18.00', '600.00', 'A', '2019-01-09', null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('120', '1', '07', 'FC01', '#', '2019-01-09 15:05:27', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-17', '1e45d277-060b-4a17-92f7-a1e3878cd5ee', 'http://apifacturaloperu.com/downloads/document/xml/1e45d277-060b-4a17-92f7-a1e3878cd5ee', 'http://apifacturaloperu.com/downloads/document/pdf/1e45d277-060b-4a17-92f7-a1e3878cd5ee', 'http://apifacturaloperu.com/downloads/document/cdr/1e45d277-060b-4a17-92f7-a1e3878cd5ee', 'La Nota de Credito numero FC01-17, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('121', '1', '07', 'FC01', '#', '2019-01-09 15:05:56', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-18', '400ec61a-4ce9-4943-91b1-e4be2fd88fff', 'http://apifacturaloperu.com/downloads/document/xml/400ec61a-4ce9-4943-91b1-e4be2fd88fff', 'http://apifacturaloperu.com/downloads/document/pdf/400ec61a-4ce9-4943-91b1-e4be2fd88fff', 'http://apifacturaloperu.com/downloads/document/cdr/400ec61a-4ce9-4943-91b1-e4be2fd88fff', 'La Nota de Credito numero FC01-18, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('122', '1', '07', 'FC01', '#', '2019-01-09 15:07:56', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-19', '04bbc44e-8997-481c-af3c-79c4d793cd28', 'http://apifacturaloperu.com/downloads/document/xml/04bbc44e-8997-481c-af3c-79c4d793cd28', 'http://apifacturaloperu.com/downloads/document/pdf/04bbc44e-8997-481c-af3c-79c4d793cd28', 'http://apifacturaloperu.com/downloads/document/cdr/04bbc44e-8997-481c-af3c-79c4d793cd28', 'La Nota de Credito numero FC01-19, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('123', '1', '07', 'FC01', '#', '2019-01-09 15:09:02', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-20', 'bdc6c6a7-838f-40ff-8d5e-55fb0017b176', 'http://apifacturaloperu.com/downloads/document/xml/bdc6c6a7-838f-40ff-8d5e-55fb0017b176', 'http://apifacturaloperu.com/downloads/document/pdf/bdc6c6a7-838f-40ff-8d5e-55fb0017b176', 'http://apifacturaloperu.com/downloads/document/cdr/bdc6c6a7-838f-40ff-8d5e-55fb0017b176', 'La Nota de Credito numero FC01-20, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('124', '1', '08', 'FC01', '#', '2019-01-09 15:09:48', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-3', 'b67efc3b-8173-4801-97e5-289db649935f', 'http://apifacturaloperu.com/downloads/document/xml/b67efc3b-8173-4801-97e5-289db649935f', 'http://apifacturaloperu.com/downloads/document/pdf/b67efc3b-8173-4801-97e5-289db649935f', 'http://apifacturaloperu.com/downloads/document/cdr/b67efc3b-8173-4801-97e5-289db649935f', 'La Nota de Debito numero FC01-3, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('125', '1', '07', 'FC01', '#', '2019-01-09 15:10:35', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-21', '2eed8fac-bcf2-4218-95b5-a7ff680b775d', 'http://apifacturaloperu.com/downloads/document/xml/2eed8fac-bcf2-4218-95b5-a7ff680b775d', 'http://apifacturaloperu.com/downloads/document/pdf/2eed8fac-bcf2-4218-95b5-a7ff680b775d', '', 'Boleta FC01-21 generada correctamente', null);
INSERT INTO `venta` VALUES ('126', '1', '01', 'F001', '#', '2019-01-09 15:10:51', '18.00', '600.00', 'A', '2019-01-09', null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('127', '1', '01', 'F001', '#', '2019-01-09 15:11:07', '18.00', '600.00', 'A', '2019-01-09', '1', 'F001-12', '5d88ad90-2563-458d-8a1d-3eff2d04b2bc', 'http://apifacturaloperu.com/downloads/document/xml/5d88ad90-2563-458d-8a1d-3eff2d04b2bc', 'http://apifacturaloperu.com/downloads/document/pdf/5d88ad90-2563-458d-8a1d-3eff2d04b2bc', 'http://apifacturaloperu.com/downloads/document/cdr/5d88ad90-2563-458d-8a1d-3eff2d04b2bc', 'La Factura numero F001-12, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('128', '1', '07', 'FC01', '#', '2019-01-09 15:11:20', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-22', 'cdce5d85-da21-47e7-9c7d-f656f710c4fd', 'http://apifacturaloperu.com/downloads/document/xml/cdce5d85-da21-47e7-9c7d-f656f710c4fd', 'http://apifacturaloperu.com/downloads/document/pdf/cdce5d85-da21-47e7-9c7d-f656f710c4fd', 'http://apifacturaloperu.com/downloads/document/cdr/cdce5d85-da21-47e7-9c7d-f656f710c4fd', 'La Nota de Credito numero FC01-22, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('129', '1', '01', 'F001', '#', '2019-01-09 15:24:36', '18.00', '600.00', 'A', '2019-01-09', '1', 'F001-14', '05754add-759a-4038-b6aa-38008356fa47', 'http://apifacturaloperu.com/downloads/document/xml/05754add-759a-4038-b6aa-38008356fa47', 'http://apifacturaloperu.com/downloads/document/pdf/05754add-759a-4038-b6aa-38008356fa47', 'http://apifacturaloperu.com/downloads/document/cdr/05754add-759a-4038-b6aa-38008356fa47', 'La Factura numero F001-14, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('130', '1', '07', 'FC01', '#', '2019-01-09 15:24:50', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-23', '5bae6bce-a591-40f4-bbd9-2faf648b23aa', 'http://apifacturaloperu.com/downloads/document/xml/5bae6bce-a591-40f4-bbd9-2faf648b23aa', 'http://apifacturaloperu.com/downloads/document/pdf/5bae6bce-a591-40f4-bbd9-2faf648b23aa', 'http://apifacturaloperu.com/downloads/document/cdr/5bae6bce-a591-40f4-bbd9-2faf648b23aa', 'La Nota de Credito numero FC01-23, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('131', '1', '01', 'F001', '#', '2019-01-09 15:27:21', '18.00', '600.00', 'A', '2019-01-09', '1', 'F001-15', '848d9e2b-c5c7-44a9-99cf-25fb2b4d8749', 'http://apifacturaloperu.com/downloads/document/xml/848d9e2b-c5c7-44a9-99cf-25fb2b4d8749', 'http://apifacturaloperu.com/downloads/document/pdf/848d9e2b-c5c7-44a9-99cf-25fb2b4d8749', 'http://apifacturaloperu.com/downloads/document/cdr/848d9e2b-c5c7-44a9-99cf-25fb2b4d8749', 'La Factura numero F001-15, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('132', '1', '08', 'FD01', '#', '2019-01-09 15:27:42', '18.00', '800.00', 'A', '2019-01-09', null, null, null, null, null, null, null, null);
INSERT INTO `venta` VALUES ('133', '1', '08', 'FD01', '#', '2019-01-09 15:28:18', '18.00', '600.00', 'A', '2019-01-09', '1', 'FD01-3', '1bfe8747-a9d0-4e21-89f3-b064c76706c7', 'http://apifacturaloperu.com/downloads/document/xml/1bfe8747-a9d0-4e21-89f3-b064c76706c7', 'http://apifacturaloperu.com/downloads/document/pdf/1bfe8747-a9d0-4e21-89f3-b064c76706c7', 'http://apifacturaloperu.com/downloads/document/cdr/1bfe8747-a9d0-4e21-89f3-b064c76706c7', 'La Nota de Debito numero FD01-3, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('134', '1', '08', 'FD01', '#', '2019-01-09 15:28:38', '18.00', '1000.00', 'A', '2019-01-09', '1', 'FD01-4', 'ce059867-804a-48e4-8b1e-70ee1302090b', 'http://apifacturaloperu.com/downloads/document/xml/ce059867-804a-48e4-8b1e-70ee1302090b', 'http://apifacturaloperu.com/downloads/document/pdf/ce059867-804a-48e4-8b1e-70ee1302090b', 'http://apifacturaloperu.com/downloads/document/cdr/ce059867-804a-48e4-8b1e-70ee1302090b', 'La Nota de Debito numero FD01-4, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('135', '1', '03', 'B001', '#', '2019-01-09 16:20:29', '18.00', '600.00', 'A', '2019-01-09', '1', 'B001-6', 'dc72dfe1-64c5-40db-b547-a7d3a0df2003', 'http://apifacturaloperu.com/downloads/document/xml/dc72dfe1-64c5-40db-b547-a7d3a0df2003', 'http://apifacturaloperu.com/downloads/document/pdf/dc72dfe1-64c5-40db-b547-a7d3a0df2003', '', 'Boleta B001-6 generada correctamente', null);
INSERT INTO `venta` VALUES ('136', '1', '08', 'FD01', '#', '2019-01-09 16:20:56', '18.00', '800.00', 'A', '2019-01-09', '1', 'FD01-5', '1b24c647-f8ce-4a40-8a7a-bdb580eb9efa', 'http://apifacturaloperu.com/downloads/document/xml/1b24c647-f8ce-4a40-8a7a-bdb580eb9efa', 'http://apifacturaloperu.com/downloads/document/pdf/1b24c647-f8ce-4a40-8a7a-bdb580eb9efa', '', 'Boleta FD01-5 generada correctamente', null);
INSERT INTO `venta` VALUES ('137', '1', '07', 'FC01', '#', '2019-01-09 16:23:01', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-25', '3b6fb37e-eb35-4622-bf5f-272adb2c8206', 'http://apifacturaloperu.com/downloads/document/xml/3b6fb37e-eb35-4622-bf5f-272adb2c8206', 'http://apifacturaloperu.com/downloads/document/pdf/3b6fb37e-eb35-4622-bf5f-272adb2c8206', '', 'Documento FC01-25 generado correctamente', null);
INSERT INTO `venta` VALUES ('138', '1', '08', 'FD01', '#', '2019-01-09 16:23:24', '18.00', '600.00', 'A', '2019-01-09', '1', 'FD01-6', '137b7d29-cc14-4c8a-9b9c-8997d3af443d', 'http://apifacturaloperu.com/downloads/document/xml/137b7d29-cc14-4c8a-9b9c-8997d3af443d', 'http://apifacturaloperu.com/downloads/document/pdf/137b7d29-cc14-4c8a-9b9c-8997d3af443d', '', 'Documento FD01-6 generado correctamente', null);
INSERT INTO `venta` VALUES ('139', '1', '01', 'F001', '#', '2019-01-09 16:24:47', '18.00', '600.00', 'A', '2019-01-09', '1', 'F001-16', '58d278c1-f61a-4f6e-825e-8d1180919ca3', 'http://apifacturaloperu.com/downloads/document/xml/58d278c1-f61a-4f6e-825e-8d1180919ca3', 'http://apifacturaloperu.com/downloads/document/pdf/58d278c1-f61a-4f6e-825e-8d1180919ca3', 'http://apifacturaloperu.com/downloads/document/cdr/58d278c1-f61a-4f6e-825e-8d1180919ca3', 'La Factura numero F001-16, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('140', '1', '03', 'B001', '#', '2019-01-09 16:25:34', '18.00', '200.00', 'A', '2019-01-09', '1', 'B001-7', '05986e51-f1b1-4f05-85af-a06ec61c9438', 'http://apifacturaloperu.com/downloads/document/xml/05986e51-f1b1-4f05-85af-a06ec61c9438', 'http://apifacturaloperu.com/downloads/document/pdf/05986e51-f1b1-4f05-85af-a06ec61c9438', '', 'Documento B001-7 generado correctamente', null);
INSERT INTO `venta` VALUES ('141', '1', '03', 'B001', '#', '2019-01-09 16:33:47', '18.00', '200.00', 'A', '2019-01-09', '1', 'B001-13', '49b4fadf-1141-49d8-b9e1-e0f5528dfb17', 'http://apifacturaloperu.com/downloads/document/xml/49b4fadf-1141-49d8-b9e1-e0f5528dfb17', 'http://apifacturaloperu.com/downloads/document/pdf/49b4fadf-1141-49d8-b9e1-e0f5528dfb17', '', 'Documento B001-13 generado correctamente', null);
INSERT INTO `venta` VALUES ('142', '1', '01', 'F001', '#', '2019-01-09 16:36:24', '18.00', '200.00', 'A', '2019-01-09', '1', 'F001-17', '2469ea38-f73d-47f4-bebd-5bdb46638f4b', 'http://apifacturaloperu.com/downloads/document/xml/2469ea38-f73d-47f4-bebd-5bdb46638f4b', 'http://apifacturaloperu.com/downloads/document/pdf/2469ea38-f73d-47f4-bebd-5bdb46638f4b', 'http://apifacturaloperu.com/downloads/document/cdr/2469ea38-f73d-47f4-bebd-5bdb46638f4b', 'La Factura numero F001-17, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('143', '1', '01', 'F001', '#', '2019-01-09 16:37:50', '18.00', '600.00', 'A', '2019-01-09', '1', 'F001-18', 'ceeff4f0-b054-4627-a959-1534803c4bdd', 'http://apifacturaloperu.com/downloads/document/xml/ceeff4f0-b054-4627-a959-1534803c4bdd', 'http://apifacturaloperu.com/downloads/document/pdf/ceeff4f0-b054-4627-a959-1534803c4bdd', 'http://apifacturaloperu.com/downloads/document/cdr/ceeff4f0-b054-4627-a959-1534803c4bdd', 'La Factura numero F001-18, ha sido aceptada', null);
INSERT INTO `venta` VALUES ('144', '1', '07', 'FC01', '#', '2019-01-09 16:38:03', '18.00', '600.00', 'A', '2019-01-09', '1', 'FC01-26', 'ccffb042-bd03-4abf-9981-115be8467136', 'http://apifacturaloperu.com/downloads/document/xml/ccffb042-bd03-4abf-9981-115be8467136', 'http://apifacturaloperu.com/downloads/document/pdf/ccffb042-bd03-4abf-9981-115be8467136', 'http://apifacturaloperu.com/downloads/document/cdr/ccffb042-bd03-4abf-9981-115be8467136', 'La Nota de Credito numero FC01-26, ha sido aceptada', '1');
INSERT INTO `venta` VALUES ('145', '1', '03', 'B001', '#', '2019-01-09 16:40:08', '0.00', '1200.00', 'A', '2019-01-09', '1', 'B001-14', 'cde9fcf8-0330-47d3-a105-6f0a1c47d3d7', 'http://apifacturaloperu.com/downloads/document/xml/cde9fcf8-0330-47d3-a105-6f0a1c47d3d7', 'http://apifacturaloperu.com/downloads/document/pdf/cde9fcf8-0330-47d3-a105-6f0a1c47d3d7', '', 'Documento B001-14 generado correctamente', null);
INSERT INTO `venta` VALUES ('146', '1', '07', 'FC01', '#', '2019-01-09 16:40:19', '18.00', '1200.00', 'A', '2019-01-09', '1', 'FC01-27', 'f6132e42-b617-404e-886f-3d55e24126b6', 'http://apifacturaloperu.com/downloads/document/xml/f6132e42-b617-404e-886f-3d55e24126b6', 'http://apifacturaloperu.com/downloads/document/pdf/f6132e42-b617-404e-886f-3d55e24126b6', '', 'Documento FC01-27 generado correctamente', null);
SET FOREIGN_KEY_CHECKS=1;
