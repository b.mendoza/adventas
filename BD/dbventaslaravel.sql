﻿# Host: localhost  (Version 5.5.5-10.1.32-MariaDB)
# Date: 2019-01-07 11:17:45
# Generator: MySQL-Front 6.0  (Build 1.82)


#
# Structure for table "categoria"
#

DROP TABLE IF EXISTS `categoria`;
CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) COLLATE utf8_spanish2_ci NOT NULL,
  `descripcion` varchar(256) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `condicion` tinyint(1) NOT NULL,
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

#
# Data for table "categoria"
#

INSERT INTO `categoria` VALUES (1,'Equipos Cómputo','Accesorios de cómputo',1),(2,'Útiles','Útiles',1),(3,'Limpieza','Artículos de limpieza',1),(4,'Medicina','Artículos medicinales',1),(5,'Líquidos','Líquidos',1),(6,'Comida','productos de comida',1),(7,'Vestimenta','Artículos de vestimenta',1),(8,'Servicios','Servicios',1),(9,'Cables','Todo tipo de cables',1),(10,'Accesorios de Sonido 2','Todos los accesorios de sonido 2',0),(11,'fgj1','fjghf2',0),(12,'hjkhk2','khykuyh',0),(13,'qqqqqq','wwww',0),(14,'Tecnología','Artículos de cómputo',1),(15,'Jabón líquilo','Productos de Aseo',1),(16,'Hoja Bón','Artículos de Oficina',1);

#
# Structure for table "articulo"
#

DROP TABLE IF EXISTS `articulo`;
CREATE TABLE `articulo` (
  `idarticulo` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `descripcion` varchar(512) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idarticulo`),
  KEY `fk_articulo_categoria_idx` (`idcategoria`),
  CONSTRAINT `fk_articulo_categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

#
# Data for table "articulo"
#

INSERT INTO `articulo` VALUES (1,1,'7701234000011','Impresora Epson Lx200',16,'','impresora.jpg','Activo'),(2,1,'7702004003508','Impresora Epson M300',19,'','Impresora Epson.jpeg','Activo'),(4,3,'5901234123457','Cable UTP Cat-5',89,'','descarga.jpg','Activo'),(5,3,'8412345678905','Cable VGA 2Mt',52,'','images.jpg','Activo');

#
# Structure for table "migrations"
#

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "migrations"
#

INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1);

#
# Structure for table "password_resets"
#

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "password_resets"
#


#
# Structure for table "persona"
#

DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `idpersona` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_persona` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish2_ci NOT NULL,
  `tipo_documento` varchar(20) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `num_documento` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `direccion` varchar(70) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `telefono` varchar(15) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `email` varchar(50) COLLATE utf8_spanish2_ci DEFAULT NULL,
  PRIMARY KEY (`idpersona`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

#
# Data for table "persona"
#

INSERT INTO `persona` VALUES (1,'Cliente','Ana Montenegro','DNI','74125863','Margaritas 1368 - Chiclayo','',''),(2,'Inactivo','Juan Perez','DNI','36985214',NULL,NULL,NULL),(3,'Cliente','Jose Martinez','PAS','0174589632','José Gálvez 1368 - Trujillo','96325871','jose@gmail.com'),(4,'Cliente','Juan Carlos Arcila','DNI','47715777','José Gálvez 1368','','jcarlos.ad7@gmail.com'),(5,'Proveedor','Soluciones Innovadoras Perú S.A.C','RUC','20600121234','Chiclayo 0123','931742904','informes.solinperu@gmail.com'),(6,'Proveedor','Inversiones SantaAna S.A.C','RUC','20546231478','Chongoyape 01','074963258','santaana@gmail.com'),(7,'Cliente','Luis Cesar Tafur Torres','DNI','48124556','PS7 MZ. N LT. 5 AH. LAMPA DE ORO VENTANILLA- CALLAO 06 CALLAO,','924899800','ingenieroluistafur@gmail.com');

#
# Structure for table "ingreso"
#

DROP TABLE IF EXISTS `ingreso`;
CREATE TABLE `ingreso` (
  `idingreso` int(11) NOT NULL AUTO_INCREMENT,
  `idproveedor` int(11) NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8_spanish2_ci DEFAULT NULL,
  `num_comprobante` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idingreso`),
  KEY `fk_ingreso_persona_idx` (`idproveedor`),
  CONSTRAINT `fk_ingreso_persona` FOREIGN KEY (`idproveedor`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

#
# Data for table "ingreso"
#

INSERT INTO `ingreso` VALUES (3,5,'Boleta','001','001','2016-08-19 22:19:34',18.00,'A'),(4,6,'Boleta','001','0001','2016-08-30 16:19:48',18.00,'A'),(5,5,'Factura','001','00004','2016-08-30 16:29:00',18.00,'A'),(6,5,'Boleta','001','0001','2016-08-31 11:02:43',18.00,'A'),(7,5,'Boleta','001','0008','2016-08-31 11:22:33',18.00,'A'),(8,5,'Boleta','001','0001','2016-08-31 11:37:20',18.00,'A'),(9,5,'Boleta','007','0008','2016-08-31 15:37:16',18.00,'A'),(10,6,'Factura','001','001','2016-09-01 16:33:03',18.00,'A'),(11,5,'Boleta','001','0003','2016-09-01 17:00:51',18.00,'A'),(12,5,'Boleta','001','00077','2016-09-27 15:19:58',18.00,'A'),(13,5,'Boleta','001','00002','2016-09-29 16:12:58',18.00,'A'),(14,5,'Boleta','001','0002','2016-10-02 13:42:22',0.00,'A'),(15,5,'Boleta','002','0003','2016-10-02 13:43:51',18.00,'A'),(16,5,'Ticket','001','0005','2016-10-02 13:44:13',0.00,'A'),(17,5,'Boleta','001','000155','2016-11-01 10:38:48',18.00,'A'),(18,6,'Boleta','0009','123','2018-02-19 21:08:22',18.00,'A'),(19,6,'Boleta','001','2545','2018-06-05 14:42:23',18.00,'C'),(20,5,'Boleta','001','4525','2018-06-05 14:43:09',18.00,'A'),(21,5,'Boleta','0002','2545','2018-06-05 14:46:14',18.00,'A'),(22,5,'Boleta','001','4525','2018-06-20 13:56:38',18.00,'A'),(23,6,'Boleta','0001','741','2019-01-07 04:01:43',18.00,'A');

#
# Structure for table "detalle_ingreso"
#

DROP TABLE IF EXISTS `detalle_ingreso`;
CREATE TABLE `detalle_ingreso` (
  `iddetalle_ingreso` int(11) NOT NULL AUTO_INCREMENT,
  `idingreso` int(11) NOT NULL,
  `idarticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_compra` decimal(11,2) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  PRIMARY KEY (`iddetalle_ingreso`),
  KEY `fk_detalle_ingreso_idx` (`idingreso`),
  KEY `fk_detalle_ingreso_articulo_idx` (`idarticulo`),
  CONSTRAINT `fk_detalle_ingreso` FOREIGN KEY (`idingreso`) REFERENCES `ingreso` (`idingreso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_ingreso_articulo` FOREIGN KEY (`idarticulo`) REFERENCES `articulo` (`idarticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

#
# Data for table "detalle_ingreso"
#

INSERT INTO `detalle_ingreso` VALUES (18,13,1,10,500.00,600.00),(19,13,4,100,1.00,2.00),(20,13,5,50,10.00,15.00),(21,14,1,1,10.00,15.00),(22,15,1,1,10.00,15.00),(23,16,1,1,10.00,15.00),(24,17,2,10,150.00,200.00),(25,18,2,10,5.00,10.00),(26,19,2,1,25.00,30.00),(27,20,1,1,20.00,30.00),(28,20,2,1,20.00,25.00),(29,21,1,10,10.00,25.00),(30,22,2,2,20.00,30.00),(31,23,5,10,25.00,30.00);

#
# Structure for table "users"
#

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

#
# Data for table "users"
#

INSERT INTO `users` VALUES (3,'Luis','luistafur@gmail.com','$2y$10$LT67nTsfiL6YPcvmBnKV6OpOFrb3r4L6gSd6czYMQYhIpcH6Ys5Gm','8Z20fQVOacHIcbDVCaIgJXf4gynAYTF9Crjup5ldiVd6Fq27QstDUAtXZwAv','2016-10-25 02:37:20','2019-01-07 05:17:29');

#
# Structure for table "venta"
#

DROP TABLE IF EXISTS `venta`;
CREATE TABLE `venta` (
  `idventa` int(11) NOT NULL AUTO_INCREMENT,
  `idcliente` int(11) NOT NULL,
  `tipo_comprobante` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  `serie_comprobante` varchar(7) COLLATE utf8_spanish2_ci NOT NULL,
  `num_comprobante` varchar(10) COLLATE utf8_spanish2_ci NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `impuesto` decimal(4,2) NOT NULL,
  `total_venta` decimal(11,2) NOT NULL,
  `estado` varchar(20) COLLATE utf8_spanish2_ci NOT NULL,
  PRIMARY KEY (`idventa`),
  KEY `fk_venta_cliente_idx` (`idcliente`),
  CONSTRAINT `fk_venta_cliente` FOREIGN KEY (`idcliente`) REFERENCES `persona` (`idpersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

#
# Data for table "venta"
#

INSERT INTO `venta` VALUES (1,1,'Boleta','001','0001','2016-09-01 00:00:00',18.00,120.00,'A'),(3,1,'Boleta','001','0005','2016-09-28 15:42:43',18.00,303.60,'A'),(4,1,'Boleta','001','0005','2016-09-28 15:43:02',18.00,303.60,'A'),(5,3,'Boleta','001','0005','2016-09-28 15:43:17',18.00,6.60,'C'),(6,6,'Boleta','001','0002','2016-09-28 15:44:02',18.00,355.30,'A'),(7,1,'Boleta','001','00002','2016-09-28 16:16:35',18.00,25.00,'A'),(8,1,'Boleta','007','777','2016-09-28 16:18:15',18.00,16512.80,'C'),(9,4,'Boleta','001','00005','2016-09-28 22:23:28',18.00,31.60,'A'),(10,3,'Factura','001','0008','2016-09-29 16:54:13',18.00,580.00,'A'),(11,1,'Factura','001','0005','2016-10-02 13:46:13',18.00,15.00,'A'),(12,1,'Boleta','001','0007','2016-10-02 13:46:38',0.00,15.00,'A'),(13,3,'Factura','007','00077','2016-10-02 14:25:12',18.00,40.00,'A'),(14,1,'Factura','001','00010','2016-11-01 10:39:26',18.00,161.25,'A'),(15,4,'Boleta','002','123','2018-02-19 21:07:24',0.00,1000.00,'A'),(16,3,'Ticket','001','2545','2018-06-05 14:44:21',18.00,135.00,'A'),(17,1,'Boleta','002','2545','2018-06-05 14:46:45',0.00,116.67,'C'),(18,1,'Boleta','001','1452','2018-06-20 13:54:00',18.00,4.00,'A'),(19,7,'Boleta','001','123','2019-01-07 03:57:14',18.00,250.33,'C'),(20,1,'Boleta','001','125','2019-01-07 04:11:32',18.00,239.33,'A');

#
# Structure for table "detalle_venta"
#

DROP TABLE IF EXISTS `detalle_venta`;
CREATE TABLE `detalle_venta` (
  `iddetalle_venta` int(11) NOT NULL AUTO_INCREMENT,
  `idventa` int(11) NOT NULL,
  `idarticulo` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio_venta` decimal(11,2) NOT NULL,
  `descuento` decimal(11,2) NOT NULL,
  PRIMARY KEY (`iddetalle_venta`),
  KEY `fk_detalle_venta_articulo_idx` (`idarticulo`),
  KEY `fk_detalle_venta_idx` (`idventa`),
  CONSTRAINT `fk_detalle_venta` FOREIGN KEY (`idventa`) REFERENCES `venta` (`idventa`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_detalle_venta_articulo` FOREIGN KEY (`idarticulo`) REFERENCES `articulo` (`idarticulo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

#
# Data for table "detalle_venta"
#

INSERT INTO `detalle_venta` VALUES (1,1,1,10,15.00,0.00),(2,4,2,1,303.60,0.00),(3,5,4,1,6.60,0.00),(4,6,1,1,52.60,0.60),(5,6,2,1,303.60,0.30),(6,7,5,1,25.00,0.00),(7,8,4,37,6.60,0.00),(8,8,5,18,25.00,0.00),(9,8,2,45,303.60,0.00),(10,8,1,41,52.60,0.00),(11,9,5,1,25.00,0.00),(12,9,4,1,6.60,0.00),(13,10,5,2,15.00,0.00),(14,10,1,1,600.00,50.00),(15,11,5,1,15.00,0.00),(16,12,5,1,15.00,0.00),(17,13,5,2,15.00,0.00),(18,13,4,5,2.00,0.00),(19,14,1,1,161.25,0.00),(20,15,2,5,200.00,5.00),(21,16,1,1,135.00,0.00),(22,17,1,1,116.67,0.00),(23,18,4,2,2.00,0.00),(24,19,1,2,116.67,0.00),(25,19,5,2,15.00,0.00),(26,19,4,1,2.00,0.00),(27,20,1,2,116.67,0.00),(28,20,4,3,2.00,0.00);

#
# Trigger "tr_updStockIngreso"
#

DROP TRIGGER IF EXISTS `tr_updStockIngreso`;
dbventaslaravel

#
# Trigger "tr_updStockVenta"
#

DROP TRIGGER IF EXISTS `tr_updStockVenta`;
dbventaslaravel
