/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50719
Source Host           : localhost:3306
Source Database       : dbventaslaravel

Target Server Type    : MYSQL
Target Server Version : 50719
File Encoding         : 65001

Date: 2019-01-08 16:43:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for summaries
-- ----------------------------
DROP TABLE IF EXISTS `summaries`;
CREATE TABLE `summaries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) DEFAULT NULL,
  `date_of_issue` date DEFAULT NULL,
  `date_of_reference` date DEFAULT NULL,
  `ticket` varchar(255) DEFAULT NULL,
  `have_cdr` varchar(255) DEFAULT NULL,
  `have_xml` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `success` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of summaries
-- ----------------------------
INSERT INTO `summaries` VALUES ('1', 'd64cd984-17aa-4c57-b985-ab5b1607c35a', '2019-01-08', '2019-01-08', '1546977236053', 'http://apifacturaloperu.com/downloads/summary/cdr/d64cd984-17aa-4c57-b985-ab5b1607c35a', 'http://apifacturaloperu.com/downloads/summary/xml/d64cd984-17aa-4c57-b985-ab5b1607c35a', 'El Resumen diario RC-20190108-1, ha sido aceptado', '1');
INSERT INTO `summaries` VALUES ('2', '71f4d587-43b9-4c2b-b267-21b26307561b', '2019-01-08', '2019-01-08', '1546977450574', 'http://apifacturaloperu.com/downloads/summary/cdr/71f4d587-43b9-4c2b-b267-21b26307561b', 'http://apifacturaloperu.com/downloads/summary/xml/71f4d587-43b9-4c2b-b267-21b26307561b', 'El Resumen diario RC-20190108-2, ha sido aceptado', '1');
INSERT INTO `summaries` VALUES ('3', '32a108f8-de74-44cc-bf52-5cb412b3ab87', '2019-01-08', '2019-01-08', '1546978045050', 'http://apifacturaloperu.com/downloads/summary/cdr/32a108f8-de74-44cc-bf52-5cb412b3ab87', 'http://apifacturaloperu.com/downloads/summary/xml/32a108f8-de74-44cc-bf52-5cb412b3ab87', 'El Resumen diario RC-20190108-8, ha sido aceptado', '1');
INSERT INTO `summaries` VALUES ('4', '003bfaf2-e343-43d6-b918-12b263109f03', '2019-01-08', '2019-01-08', '1546978068464', 'http://apifacturaloperu.com/downloads/summary/cdr/003bfaf2-e343-43d6-b918-12b263109f03', 'http://apifacturaloperu.com/downloads/summary/xml/003bfaf2-e343-43d6-b918-12b263109f03', 'El Resumen diario RC-20190108-9, ha sido aceptado', '1');
INSERT INTO `summaries` VALUES ('5', '71d16be3-b6e7-41bd-9d3d-8b2fd2ce4740', '2019-01-08', '2019-01-08', '1546978147770', 'http://apifacturaloperu.com/downloads/summary/cdr/71d16be3-b6e7-41bd-9d3d-8b2fd2ce4740', 'http://apifacturaloperu.com/downloads/summary/xml/71d16be3-b6e7-41bd-9d3d-8b2fd2ce4740', 'El Resumen diario RC-20190108-10, ha sido aceptado', '1');
INSERT INTO `summaries` VALUES ('6', '830b10c2-8e5e-48b4-9905-2e8dba6b83ef', '2019-01-08', '2019-01-08', '1546979556880', 'http://apifacturaloperu.com/downloads/summary/cdr/830b10c2-8e5e-48b4-9905-2e8dba6b83ef', 'http://apifacturaloperu.com/downloads/summary/xml/830b10c2-8e5e-48b4-9905-2e8dba6b83ef', 'El Resumen diario RC-20190108-12, ha sido aceptado', '1');
INSERT INTO `summaries` VALUES ('7', '4723a46c-8e04-4382-9f06-f9fa0173e830', '2019-01-08', '2019-01-08', '1546979603340', 'http://apifacturaloperu.com/downloads/summary/cdr/4723a46c-8e04-4382-9f06-f9fa0173e830', 'http://apifacturaloperu.com/downloads/summary/xml/4723a46c-8e04-4382-9f06-f9fa0173e830', 'El Resumen diario RC-20190108-13, ha sido aceptado', '1');
SET FOREIGN_KEY_CHECKS=1;
