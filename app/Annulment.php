<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Annulment extends Model
{
    public $timestamps=false;


    protected $fillable =[
    	'id',
    	'external_id', 
    	'date_of_issue',
    	'date_of_reference',
    	'ticket',
    	'have_cdr',
    	'response',
    	'success',
        'have_xml',
        'document_affected',
        'type_document_affected'
    ];
}
