<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Venta extends Model
{
    protected $table='venta';

    protected $primaryKey='idventa';

    public $timestamps=false;

    protected $fillable =[
    	'idcliente',
    	'tipo_comprobante',
    	'serie_comprobante',
    	'num_comprobante',
    	'fecha_hora',
    	'impuesto',
    	'total_venta',
		'estado',		
		'annulment',
    	'number',
    	'external_id',
    	'group_id',
    	'have_xml',
    	'have_pdf',
    	'have_cdr',
    	'response'
    ];
    protected $guarded =[
	];
	
	public function detalleventa()
    {
        return $this->hasMany(DetalleVenta::class,'idventa','idventa');
	}

	public function notes()
    {
        return $this->hasMany(Note::class,'venta_id','venta_id');
	}
	
	public function persona()
    {
        return $this->belongsTo(Persona::class,'idcliente','idpersona');
    }
}
