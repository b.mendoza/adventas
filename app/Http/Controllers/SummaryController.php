<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Company;
use sisVentas\Summary;
use sisVentas\Venta;

class SummaryController extends Controller
{
    protected $company;
     
    public function index()
    {
        $summaries = Summary::orderBy('id','desc')->paginate(7);
        return view('ventas.summary.index', compact('summaries'));
    }

     
    public function create()
    {
        return view('ventas.summary.create');
    }

    
    public function store(Request $request)
    {

        $array = ["fecha_de_emision_de_documentos" =>$request->input('date_search'), "codigo_tipo_proceso" => "1" ];        
        $message = "";        
        $result = json_decode($this->send_summary_or_consult_ticket(json_encode($array),1),true);
  
        if($result['success']){

            $summary = new Summary;
            $summary->external_id = $result['data']['external_id'];
            $summary->date_of_issue = date('Y-m-d');
            $summary->date_of_reference = $request->input('date_search');
            $summary->ticket = $result['data']['ticket']; 
            $summary->save();
            $message = "Resúmen generado correctamente, consulte el ticket"; 

        }else{
            $message = $result['message'];
        }

        return redirect('summary')->with('success', $message);                    

    }





    public function consultTicket(Request $request)
    {
        
        $summary = Summary::find($request->input('id'));

        $array = [ "external_id" => $summary->external_id,"ticket" => $summary->ticket ];        
        $message = "";
        $result = json_decode($this->send_summary_or_consult_ticket(json_encode($array),0),true);
 
 
        if($result['success']){ 
 
            $summary->have_cdr = $result['links']['cdr']; 
            $summary->have_xml = $result['links']['xml']; 
            $summary->response = $result['response']['description']; 
            $summary->number = substr($result['data']['filename'],12); 
            $summary->success = $result['success']; 
            $summary->save();

            $this->updateDocumentSendSummary($summary->date_of_reference);

            $message = $result['response']['description']; 

        }else{
            $message = $result['message'];
            $summary->response = $message;
            $summary->success = $result['success']; 
        }       

        return redirect('summary')->with('success', $message);                    

    }


    public function updateDocumentSendSummary($date_of_reference){

        $ventas = Venta::where('group_id','02')
                       ->whereIn('tipo_comprobante',['03','07','08'])
                       ->whereNull('send_summary')
                       ->where('date_of_issue',$date_of_reference)
                       ->get(); 
 
        foreach($ventas as $v){
            $v->send_summary = 1;
            $v->save();
        }
        
    }

    
 

    public function send_summary_or_consult_ticket($data,$f) {
  
        $this->company = Company::first();        
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => ($f==1) ? $this->company->domain.'/api/summaries' : $this->company->domain.'/api/summaries/status',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "", 
        CURLOPT_CONNECTTIMEOUT => 20,
        CURLOPT_TIMEOUT => 20,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->company->api_token,
            "Content-Type: application/json", 
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {  
            return $response;
        } 
    }

     
    public function show($id)
    {
        
    }

    
    public function edit($id)
    {
       
    }

    
    public function update(Request $request, $id)
    {
       
    }

    
    public function destroy($id)
    {
       
    }
}
