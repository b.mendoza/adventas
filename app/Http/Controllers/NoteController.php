<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Venta;
use sisVentas\Articulo;
use sisVentas\Annulment;
use sisVentas\Summary;
use DB;


class NoteController extends Controller
{ 

    public function create($id){ 

        $venta = Venta::with(['persona','detalleventa'=>function($query){
            $query->with(['articulo']);
        }])->find($id);

        $articulos = DB::table('articulo as art')
    		->join('detalle_ingreso as di','art.idarticulo','=','di.idarticulo')
            ->select(DB::raw('CONCAT(art.codigo, " ",art.nombre) AS articulo'),'art.idarticulo','art.stock', 'di.precio_venta as precio_promedio')
            ->where('art.estado','=','Activo')
            ->where('art.stock','>','0')
            ->groupBy('articulo','art.idarticulo','art.stock')
            ->get();

        $note_credit_types=DB::table('note_credit_types')->select('id','description')->get();
        $note_debit_types=DB::table('note_debit_types')->select('id','description')->get();
 
        $filter = ($venta['tipo_comprobante'] == "01") ? "F%":"B%";
        $series=DB::table('series')->whereIn('document_type',['07','08'])->where('number','LIKE', $filter)->get();          

        return view("ventas.venta.note", [  "venta"=>$venta,
                                            "articulos"=>$articulos,
                                            "note_credit_types"=>$note_credit_types,
                                            "note_debit_types"=>$note_debit_types,
                                            "series"=>$series
                                        ]);
 
    }     
    
    
}
