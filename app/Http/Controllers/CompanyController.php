<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Http\Requests\CompanyRequest;
use sisVentas\Company;
use Illuminate\Support\Facades\Redirect;

class CompanyController extends Controller
{
     
    public function company(){

        $company = Company::first();
        return view("seguridad.company.create",["company"=>$company]);
    }


    
    public function store(CompanyRequest $request)
    {    

        $company = Company::updateOrCreate(
            [
                'id' => 0
            ],
            [
                'codigo_del_domicilio_fiscal' => $request->input('codigo_del_domicilio_fiscal'),
                'codigo_pais' => $request->input('codigo_pais'),
                'ubigeo' => $request->input('ubigeo'),
                'direccion' => $request->input('direccion'),
                'correo_electronico' => $request->input('correo_electronico'),
                'telefono' => $request->input('telefono'),
                'api_token' => $request->input('api_token'),
                'domain' => $request->input('domain')
            ]
        );

        return Redirect::to('seguridad/company')->with('success',"Guardado correctamente");
    }
    
}
