<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;
use sisVentas\Venta;
use sisVentas\Annulment;
use sisVentas\Summary;
use sisVentas\Company;

class AnnulmentController extends Controller
{ 

    public function index(){

        $annulments = Annulment::orderBy('id','desc')->paginate(7);
        return view('ventas.annulment.index', compact('annulments'));
    }

    
    public function annulment(Request $request){

        $data = json_decode($request->input('annulment'),true);
        $motive = $request->input('motive_annulment');
        $type = $request->input('type'); 
 
        $result = $this->send_document($this->generate_json($data,$motive),$type);
 
        // var_dump($this->generate_json($data,$motive));

        // var_dump($data);
        // echo $type;
        // die("s");

        if($result['success']){

            $annulment = new Annulment;
            $annulment->external_id = isset($result['data']['external_id']) ? $result['data']['external_id']:$result['external_id'];
            $annulment->date_of_issue = date('Y-m-d');
            $annulment->date_of_reference = $data['date_of_issue'];
            $annulment->ticket = isset($result['data']['ticket'])?$result['data']['ticket']:$result['ticket']; 
            $annulment->type_document_affected = $type; 
            $annulment->document_affected = $data['number']; 
            $annulment->save();
            $message = "Documento ".$data['number'] ." anulado, consulte el ticket";

        }else{
            $message = $result['message'];
        }

        return redirect('annulment')->with('success', $message);        

    }



    public function consultTicket(Request $request)
    {

        $annulment = Annulment::find($request->input('id'));

        $type = $request->input('type'); 
        $array = ["external_id" => $annulment->external_id,"ticket" => $annulment->ticket];        
        $message = "";

        $result = json_decode($this->annulment_document(json_encode($array),$type),true); 

        if($result['success']){ 
 
            $annulment->have_cdr = $result['links']['cdr']; 
            $annulment->have_xml = $result['links']['xml']; 
            $annulment->response = $result['response']['description'];  
            $annulment->success = $result['success']; 
            $annulment->save();

            //actualiza a 1 si fue anulado
            $this->updateState($annulment->document_affected,$type);

            $message = $result['response']['description']; 

        }else{
            $message = $result['message'];
            $annulment->response = $message;
            $annulment->success = $result['success']; 
        }        

        return redirect('annulment')->with('success', $message);                    

    }



    public function updateState($dAffected, $type){  

        $venta = Venta::where('number',$dAffected)->first();
        $venta->annulment = 1;     
        $venta->save();

    }



    public function generate_json($data,$motive){  
         
        $array = array( 
            "fecha_de_emision_de_documentos" => $data['date_of_issue'],
            "codigo_tipo_proceso" => "1",
            "documentos" => [
                ["external_id" => $data['external_id'],
                "motivo_anulacion" => $motive]
            ]         
        );     
        return json_encode($array);
    }


    public function annulment_document($data,$type) {

        $company = Company::first();  
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => ($type=="03") ?   $company->domain.'/api/summaries/status': $company->domain.'/api/voided/status',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "", 
        CURLOPT_CONNECTTIMEOUT => 20,
        CURLOPT_TIMEOUT => 20,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$company->api_token,
            "Content-Type: application/json", 
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {  
            return $response;
        } 
    }

    

    public function send_document($data,$type) {

        $company = Company::first();  
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => ($type=="03") ? $company->domain.'/api/summaries': $company->domain.'/api/voided',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "", 
        CURLOPT_CONNECTTIMEOUT => 20,
        CURLOPT_TIMEOUT => 20,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$company->api_token,
            "Content-Type: application/json", 
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {  
            return json_decode($response,true);
        } 
    }


}
