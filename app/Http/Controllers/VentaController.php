<?php

namespace sisVentas\Http\Controllers;

use Illuminate\Http\Request;

use sisVentas\Http\Requests;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use sisVentas\Http\Requests\VentaFormRequest;
use sisVentas\Venta;
use sisVentas\DetalleVenta;
use sisVentas\Note; 
use sisVentas\Company;
use DB;
use Fpdf;
use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class VentaController extends Controller
{
    protected $venta;
    protected $company;


    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        if ($request)
        {
           $query=trim($request->get('searchText'));
           $ventas=DB::table('venta as v')
            ->join('persona as p','v.idcliente','=','p.idpersona')
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa') 
            ->select('v.idventa','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_venta'
                        ,'v.annulment','v.number','v.external_id','v.have_xml','v.have_pdf','v.have_cdr','v.response','v.success','v.send_summary','v.group_id')
            ->where('v.num_comprobante','LIKE','%'.$query.'%')
            ->orderBy('v.idventa','desc')
            ->groupBy('v.idventa','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado')
            ->paginate(7);

            $document_type = ['01' => 'FACTURA','03' => 'BOLETA DE VENTA','07' => 'NOTA DE CREDITO','08' => 'NOTA DE DEBITO'];

            return view('ventas.venta.index',["ventas"=>$ventas,"searchText"=>$query,"document_type"=>$document_type]);

        }
    }
    public function create()
    {
    	$personas=DB::table('persona')->where('tipo_persona','=','Cliente')->get();
    	$articulos = DB::table('articulo as art')
    		->join('detalle_ingreso as di','art.idarticulo','=','di.idarticulo')
            ->select(DB::raw('CONCAT(art.codigo, " ",art.nombre) AS articulo'),'art.idarticulo','art.stock', 'di.precio_venta as precio_promedio')
            ->where('art.estado','=','Activo')
            ->where('art.stock','>','0')
            ->groupBy('articulo','art.idarticulo','art.stock')
            ->get();

        $series=DB::table('series')->whereIn('document_type',['01','03'])->get(); 


        return view("ventas.venta.create",["personas"=>$personas,"articulos"=>$articulos,"series"=>$series]);
    }

    public function store (VentaFormRequest $request)
    { 

    	try{
            DB::beginTransaction();
            
        	$this->venta=new Venta;
	        $this->venta->idcliente=$request->get('idcliente');
	        $this->venta->tipo_comprobante=$request->get('tipo_comprobante');
	        $this->venta->serie_comprobante=$request->get('serie_comprobante');
	        // $this->venta->num_comprobante=$request->get('num_comprobante');
	        $this->venta->num_comprobante= "#";
	        $this->venta->total_venta=$request->get('total_venta');          

	        $mytime = Carbon::now('America/Lima');
	        $this->venta->fecha_hora=$mytime->toDateTimeString();
            $this->venta->date_of_issue = $mytime->toDateString();       
	        if ($request->get('impuesto')=='1')
            {
                $this->venta->impuesto='18';
            }
            else
            {
                $this->venta->impuesto='0';
            } 
	        $this->venta->estado='A';
	        $this->venta->save();

	        $idarticulo = $request->get('idarticulo');
	        $cantidad = $request->get('cantidad');
	        $descuento = $request->get('descuento');
	        $precio_venta = $request->get('precio_venta');

	        $cont = 0;

	        while($cont < count($idarticulo)){
	            $detalle = new DetalleVenta();
	            $detalle->idventa= $this->venta->idventa; 
	            $detalle->idarticulo= $idarticulo[$cont];
	            $detalle->cantidad= $cantidad[$cont];
	            $detalle->descuento= $descuento[$cont];
	            $detalle->precio_venta= $precio_venta[$cont];
	            $detalle->save();
	            $cont=$cont+1;            
            }          
            
            $this->company = Company::first();

            $this->updateSale($request);
            
            
        	DB::commit();

        }catch(\Exception $e)
        { 
          	DB::rollback();
        }

        return Redirect::to('ventas/venta')->with('success',$this->venta->response);
    }



    /*-------------Integración----------------*/

    public function updateSale($r){
 
        $result = json_decode($this->send_document($this->generate_json($this->venta,$r)),true);   
 
        if($result['success']){           

            if($this->venta->tipo_comprobante=="01"){
                $this->venta->group_id ="01";
            }else if($this->venta->tipo_comprobante=="03"){
                $this->venta->group_id ="02";
            }          

            $this->venta->success = $result['success']; 
            $this->venta->number = $result['data']['number']; 
            $this->venta->external_id = $result['data']['external_id']; 
            $this->venta->have_xml = $result['links']['xml']; 
            $this->venta->have_pdf = $result['links']['pdf']; 
            $this->venta->have_cdr = isset($result['links']['cdr']) ? $result['links']['cdr']:''; 
            $this->venta->response = isset($result['response']['description']) ? $result['response']['description']: 'Documento '.$this->venta->number.' generado correctamente';
            $this->venta->save();

            $this->createNote($r);                            

        }else{
            $this->venta->success = $result['success']; 
            $this->venta->response = $result['message'];             
        }
            
    }

    public function createNote($r){

        if( in_array($this->venta->tipo_comprobante,["07","08"])){  

            $this->verifyGroup($r);        
            $note = new Note;
            $note->venta_id = $r->input('id');
            $note->note_type = $this->venta->tipo_comprobante;
            $note->note_credit_type_id = ($this->venta->tipo_comprobante == "07") ? $r->input('tipo_nota_c') :  "";
            $note->note_debit_type_id = ($this->venta->tipo_comprobante == "08") ? $r->input('tipo_nota_d') : "";
            $note->description = $r->input('description');
            $note->affected_document = $r->input('document');
            $note->save();
            
        }  
    }

    
    public function verifyGroup($r){
        if($r->has('code')){
            if($r->input('code')=="01"){
                $this->venta->group_id ="01";
            }else if($r->input('code')=="03"){
                $this->venta->group_id ="02";
            }
            $this->venta->save();
        }  
    }



    public function generate_json($data, $r){ 

        $datetime = explode(' ',$data['fecha_hora']);
        $items = [];
        
        foreach ($data['detalleventa'] as $row) {
            $items[] = array(
                "codigo_interno" => $row['articulo']['codigo'],
                "descripcion" => $row['articulo']['nombre'],
                "codigo_producto_sunat" => "51121703",
                "unidad_de_medida" => "NIU",
                "cantidad" => $row['cantidad'],
                "valor_unitario" => round($row['precio_venta'],2),
                "codigo_tipo_precio" => "01",
                "precio_unitario" => round($row['precio_venta']*1.18,2),                    
                "codigo_tipo_afectacion_igv" => "10",
                "total_base_igv" => round($row['precio_venta']*$row['cantidad'],2),
                "porcentaje_igv" => 18,
                "total_igv" => round(($row['precio_venta']*$row['cantidad'])*0.18,2),
                "total_impuestos" => round(($row['precio_venta']*$row['cantidad'])*0.18,2),
                "total_valor_item" => round($row['precio_venta']*$row['cantidad'],2),
                "total_item" => round(($row['precio_venta']*$row['cantidad'])*1.18,2)                    
            );
        }

        $array = array( 
            "serie_documento" =>$data['serie_comprobante'],
            "numero_documento" => "#",
            "fecha_de_emision" => $datetime[0],
            "hora_de_emision" => $datetime[1],
            "codigo_tipo_operacion" => "0101",
            "codigo_tipo_documento" => $data['tipo_comprobante'] , 
            "codigo_tipo_moneda" => 'PEN',
            "fecha_de_vencimiento" => $datetime[0], 
            "datos_del_emisor" => array(
                "codigo_del_domicilio_fiscal" => $this->company->codigo_del_domicilio_fiscal,                    
                "codigo_pais" => $this->company->codigo_pais,
                "ubigeo" => $this->company->ubigeo,
                "direccion" => $this->company->direccion, 
                "correo_electronico" => $this->company->correo_electronico,
                "telefono" => $this->company->telefono
            ),
            "datos_del_cliente_o_receptor" => array(                
                "codigo_tipo_documento_identidad" => $data['persona']['tipo_documento'],
                "numero_documento" => $data['persona']['num_documento'],
                "apellidos_y_nombres_o_razon_social" => $data['persona']['nombre'],
                "codigo_pais" => $data['persona']['codigo_pais'],
                "ubigeo" => $this->company->ubigeo,
                "direccion" => $data['persona']['direccion'],
                "correo_electronico" => $data['persona']['email'],
                "telefono" => $data['persona']['telefono']
            ),
            "totales" => array( 
                "total_operaciones_gravadas" => round($data['total_venta'],2), 
                "total_igv" => round($data['total_venta']*0.18,2),
                "total_impuestos" => round($data['total_venta']*0.18,2),
                "total_valor" => round($data['total_venta'],2),
                "total_venta" => round( $data['total_venta']*1.18,2) 

            ),

            "items" => $items            
        );


        //si el doc es nota añadirle campos al json
        if(in_array($this->venta->tipo_comprobante,["07","08"])){            
            
            $doc = explode("-", $r->input('document'));          

            $note = [   
                "codigo_tipo_nota"=> ($this->venta->tipo_comprobante == "07") ? $r->input('tipo_nota_c') :  $r->input('tipo_nota_d'),            
                "motivo_o_sustento_de_nota" => $r->input('description'),
                "documento_afectado"=> [
                    "serie_documento" => $doc[0],
                    "numero_documento" => $doc[1],
                    "codigo_tipo_documento" => $r->input('code'),
                ]
            ];
            $array = array_merge($note,$array);
        }   

        return json_encode($array);

    }





    function send_document($data) {
   
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $this->company->domain.'/api/documents',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "", 
        CURLOPT_CONNECTTIMEOUT => 20,
        CURLOPT_TIMEOUT => 20,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "Authorization: Bearer ".$this->company->api_token,
            "Content-Type: application/json", 
            "cache-control: no-cache"
        ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {  
            
            return $response;
        } 
    }


    public function createAnnulmentVoided($id){

        $annulment = Venta::find($id);
        $type_document_affected = ['type' => $annulment->tipo_comprobante];
        return view('ventas.summary.annulment', compact('annulment','type_document_affected'));
        
    }

    /*-----------------------------*/




    public function show($id)
    {
    	$venta=DB::table('venta as v')
            ->join('persona as p','v.idcliente','=','p.idpersona')
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa')
            ->select('v.idventa','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_venta')
            ->where('v.idventa','=',$id)
            ->first();

        $detalles=DB::table('detalle_venta as d')
             ->join('articulo as a','d.idarticulo','=','a.idarticulo')
             ->select('a.nombre as articulo','d.cantidad','d.descuento','d.precio_venta')
             ->where('d.idventa','=',$id)
             ->get();
        return view("ventas.venta.show",["venta"=>$venta,"detalles"=>$detalles]);
    }

    public function destroy($id)
    {
    	$venta=Venta::findOrFail($id);
        $venta->Estado='C';
        $venta->update();
        return Redirect::to('ventas/venta');
    }
    public function reportec($id){
         //Obtengo los datos
        
        $venta=DB::table('venta as v')
            ->join('persona as p','v.idcliente','=','p.idpersona')
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa')
            ->select('v.idventa','v.fecha_hora','p.nombre','p.direccion','p.num_documento','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_venta')
            ->where('v.idventa','=',$id)
            ->first();

        $detalles=DB::table('detalle_venta as d')
             ->join('articulo as a','d.idarticulo','=','a.idarticulo')
             ->select('a.nombre as articulo','d.cantidad','d.descuento','d.precio_venta')
             ->where('d.idventa','=',$id)
             ->get();


        $pdf = new Fpdf();
        $pdf::AddPage();
        $pdf::SetFont('Arial','B',14);
        //Inicio con el reporte
        $pdf::SetXY(170,20);
        $pdf::Cell(0,0,utf8_decode($venta->tipo_comprobante));

        $pdf::SetFont('Arial','B',14);
        //Inicio con el reporte
        $pdf::SetXY(170,40);
        $pdf::Cell(0,0,utf8_decode($venta->serie_comprobante."-".$venta->num_comprobante));

        $pdf::SetFont('Arial','B',10);
        $pdf::SetXY(35,60);
        $pdf::Cell(0,0,utf8_decode($venta->nombre));
        $pdf::SetXY(35,69);
        $pdf::Cell(0,0,utf8_decode($venta->direccion));
        //***Parte de la derecha
        $pdf::SetXY(180,60);
        $pdf::Cell(0,0,utf8_decode($venta->num_documento));
        $pdf::SetXY(180,69);
        $pdf::Cell(0,0,substr($venta->fecha_hora,0,10));
        $total=0;

        //Mostramos los detalles
        $y=89;
        foreach($detalles as $det){
            $pdf::SetXY(20,$y);
            $pdf::MultiCell(10,0,$det->cantidad);

            $pdf::SetXY(32,$y);
            $pdf::MultiCell(120,0,utf8_decode($det->articulo));

            $pdf::SetXY(162,$y);
            $pdf::MultiCell(25,0,$det->precio_venta-$det->descuento);

            $pdf::SetXY(187,$y);
            $pdf::MultiCell(25,0,sprintf("%0.2F",(($det->precio_venta-$det->descuento)*$det->cantidad)));

            $total=$total+($det->precio_venta*$det->cantidad);
            $y=$y+7;
        }

        $pdf::SetXY(187,153);
        $pdf::MultiCell(20,0,"S/. ".sprintf("%0.2F", $venta->total_venta-($venta->total_venta*$venta->impuesto/($venta->impuesto+100))));
        $pdf::SetXY(187,160);
        $pdf::MultiCell(20,0,"S/. ".sprintf("%0.2F", ($venta->total_venta*$venta->impuesto/($venta->impuesto+100))));
        $pdf::SetXY(187,167);
        $pdf::MultiCell(20,0,"S/. ".sprintf("%0.2F", $venta->total_venta));

        $pdf::Output();
        exit;
    }
    public function reporte(){
         //Obtenemos los registros
         $registros=DB::table('venta as v')
            ->join('persona as p','v.idcliente','=','p.idpersona')
            ->join('detalle_venta as dv','v.idventa','=','dv.idventa')
            ->select('v.idventa','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado','v.total_venta')
            ->orderBy('v.idventa','desc')
            ->groupBy('v.idventa','v.fecha_hora','p.nombre','v.tipo_comprobante','v.serie_comprobante','v.num_comprobante','v.impuesto','v.estado')
            ->get();

         //Ponemos la hoja Horizontal (L)
         $pdf = new Fpdf('L','mm','A4');
         $pdf::AddPage();
         $pdf::SetTextColor(35,56,113);
         $pdf::SetFont('Arial','B',11);
         $pdf::Cell(0,10,utf8_decode("Listado Ventas"),0,"","C");
         $pdf::Ln();
         $pdf::Ln();
         $pdf::SetTextColor(0,0,0);  // Establece el color del texto 
         $pdf::SetFillColor(206, 246, 245); // establece el color del fondo de la celda 
         $pdf::SetFont('Arial','B',10); 
         //El ancho de las columnas debe de sumar promedio 190        
         $pdf::cell(35,8,utf8_decode("Fecha"),1,"","L",true);
         $pdf::cell(80,8,utf8_decode("Cliente"),1,"","L",true);
         $pdf::cell(45,8,utf8_decode("Comprobante"),1,"","L",true);
         $pdf::cell(10,8,utf8_decode("Imp"),1,"","C",true);
         $pdf::cell(25,8,utf8_decode("Total"),1,"","R",true);
         
         $pdf::Ln();
         $pdf::SetTextColor(0,0,0);  // Establece el color del texto 
         $pdf::SetFillColor(255, 255, 255); // establece el color del fondo de la celda
         $pdf::SetFont("Arial","",9);
         
         foreach ($registros as $reg)
         {
            $pdf::cell(35,8,utf8_decode($reg->fecha_hora),1,"","L",true);
            $pdf::cell(80,8,utf8_decode($reg->nombre),1,"","L",true);
            $pdf::cell(45,8,utf8_decode($reg->tipo_comprobante.': '.$reg->serie_comprobante.'-'.$reg->num_comprobante),1,"","L",true);
            $pdf::cell(10,8,utf8_decode($reg->impuesto),1,"","C",true);
            $pdf::cell(25,8,utf8_decode($reg->total_venta),1,"","R",true);
            $pdf::Ln(); 
         }

         $pdf::Output();
         exit;
    }
}
