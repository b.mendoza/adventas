<?php

namespace sisVentas\Http\Requests;

use sisVentas\Http\Requests\Request;

class CompanyRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'codigo_del_domicilio_fiscal'=>'required|max:20',
            'codigo_pais'=>'required|max:20',
            'ubigeo'=>'required|max:15',
            'direccion'=>'required',
            'correo_electronico'=>'required|email',
            'telefono'=>'required|numeric',
            'domain'=>'required',
            'api_token'=>'required'

        ];
    }
}
