<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Summary extends Model
{
    public $timestamps=false;


    protected $fillable =[
    	'id',
    	'external_id', 
    	'date_of_issue',
    	'number',
    	'date_of_reference',
    	'ticket',
    	'have_cdr',
    	'response',
    	'success',
		'have_xml' 
    ];
}
