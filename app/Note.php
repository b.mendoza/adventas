<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    public $timestamps=false;

    protected $fillable =[ 
    	'venta_id',
    	'note_type',
    	'note_credit_type_id',
    	'note_debit_type_id',
    	'description',
		'affected_document'
	];
	
	public function venta()
    {
        return $this->belongsTo(Venta::class);
    }
}
