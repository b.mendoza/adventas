<?php

namespace sisVentas;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{ 
    public $timestamps=false;

    protected $fillable =[ 
    	'codigo_del_domicilio_fiscal',
    	'codigo_pais',
        'ubigeo', 
        'direccion',
    	'correo_electronico',
        'domain',
    	'api_token',
    	'telefono' 
    ];

    
}
